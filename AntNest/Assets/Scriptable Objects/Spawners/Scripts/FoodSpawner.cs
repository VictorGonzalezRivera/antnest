﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New FoodSpawner Object", menuName = "Spawners/FoodSpawner")]
public class FoodSpawner : SpawnerObject
{
    private void Awake()
    {
        MaxEntities = 10;
        SpawnRadius = 20;
        EntityRadius = 2;
    }    
}
