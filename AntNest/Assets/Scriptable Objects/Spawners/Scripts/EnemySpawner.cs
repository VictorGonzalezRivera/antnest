﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New EnemySpawner", menuName = "Spawners/EnemySpawner")]
public class EnemySpawner : SpawnerObject
{
    private void Awake()
    {
        MaxEntities = 3;
        SpawnRadius = 20;
        EntityRadius = 5;
    }
}

