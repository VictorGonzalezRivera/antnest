﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class SpawnerObject : ScriptableObject
{
    [SerializeField] private GameObject entity;
    [SerializeField] private int maxEntities;
    [SerializeField] private float spawnRadius;
    [SerializeField] private float entityRadius;

    [SerializeField] private int entityCount;
    private Vector3 centerPosition;
    [SerializeField] private List<GameObject> activeList;
    [SerializeField] private List<GameObject> inactiveList;


    public GameObject Entity { get => entity; set => entity = value; }
    public int EntityCount { get => entityCount; set => entityCount = value; }
    public int MaxEntities { get => maxEntities; set => maxEntities = value; }
    public float SpawnRadius { get => spawnRadius; set => spawnRadius = value; }
    public float EntityRadius { get => entityRadius; set => entityRadius = value; }
    public Vector3 CenterPosition { get => centerPosition; set => centerPosition = value; }


    /*
    private Vector3 GetSpawnPosition()
    {
        Vector3 pos = Random.onUnitSphere;

        pos = pos.normalized * SpawnRadius;
        pos += CenterPosition;
        pos.y = 0;

        return pos;
    }*/
    
    public void Spawn(Transform t, Vector3 spawnPosition = default(Vector3), WorldController world = null)
    {
        if (EntityCount < MaxEntities)
        {
            GameObject obj = Instantiate(Entity, spawnPosition, Quaternion.identity, t);
            
            if (world && obj.GetComponent<EnemyBehaviour>()) {
                EnemyBehaviour enemy = obj.GetComponent<EnemyBehaviour>();
                enemy.wController = world;
                world.enemies.Add(enemy);
            }

            obj.SetActive(true);

            activeList.Add(obj);
            EntityCount++;
        } else if (inactiveList.Count > 0)
        {
            GameObject obj = inactiveList[0];

            obj.transform.position = spawnPosition;
            obj.SetActive(true);

            activeList.Add(obj);
            inactiveList.RemoveAt(0);
        }
        
    }

    public void SetInactive(int id, WorldController world = null)
    {
        foreach (GameObject obj in activeList)
        {
            if (obj.GetComponent<Spawnable>().Id == id)
            {
                obj.SetActive(false);

                //Si es un enemigo
                if (world && obj.GetComponent<EnemyBehaviour>()) 
                { 
                    world.enemies.Remove(obj.GetComponent<EnemyBehaviour>()); 
                }
                //

                inactiveList.Add(obj);
                activeList.Remove(obj);
                break;
            }
        }
    }

    public void ClearSpawner()
    {
        activeList.Clear();
        inactiveList.Clear();
        EntityCount = 0;
    }
}


