﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New FoodFeromoneSpawner Object", menuName = "Spawners/FoodFeromoneSpawner")]
public class FoodFeromoneSpawner : SpawnerObject
{
    private void Awake()
    {
        MaxEntities = 100;
        SpawnRadius = 0;
    }

    public void SetFeromone(int id, Vector3 direction, int index, bool inside)
    {
        Feromone F = Entity.GetComponent<Feromone>();
        F.ant_id = id;
        F.direction = direction;
        F.index = index;
        F.inside = inside;        
    }
}
