﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Ant/AntSettings", fileName = "new AntSettings")]
public class AntSettings : ScriptableObject
{
    public AntKind kind;
    [Range(0, 10)]
    public int spawnProb;
    [Range(0.0f, 1.0f)]
    public float spawnDecreasingMultiplier;

    void Start()
    {

    }
}

