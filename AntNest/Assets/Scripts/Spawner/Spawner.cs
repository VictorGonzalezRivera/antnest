﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private GameObject entity;
    [SerializeField] private int entityCount;
    public int EntityCount { get { return entityCount; } }
    [SerializeField] private int maxEntities;
    [SerializeField] private float spawnRadius;
    [SerializeField] private float spawnRate;

    private float timeNextSpawn;

    [SerializeField] private List<GameObject> activeList;
    [SerializeField] private List<GameObject> inactiveList;


    void Start()
    {
        timeNextSpawn = spawnRate;
        entity.SetActive(false);

        for (int i = 0; i < maxEntities; i++)
        {
            entityCount++;

            GameObject obj = Instantiate(entity, transform.position, Quaternion.identity);
            obj.transform.SetParent(this.transform);
            inactiveList.Add(obj);
        }
    }


    void Update()
    {
        timeNextSpawn -= Time.deltaTime;
        if (timeNextSpawn <= 0)
        {
            timeNextSpawn += spawnRate;
            if (inactiveList.Count > 0)
            {
                Vector3 spawnPosition = Random.onUnitSphere;

                spawnPosition = spawnPosition.normalized * spawnRadius;

                spawnPosition.y = 0;

                GameObject obj = inactiveList[0];

                obj.transform.position = spawnPosition;
                obj.SetActive(true);

                activeList.Add(obj);
                inactiveList.RemoveAt(0);
            }
        }
    }

    public void SetInactive(int id)
    {
        foreach (GameObject obj in activeList)
        {
            if(obj.GetComponent<Spawnable>().Id == id)
            {
                obj.SetActive(false);
                inactiveList.Add(obj);
                activeList.Remove(obj);
                break;
            }
        }   
    }


}
