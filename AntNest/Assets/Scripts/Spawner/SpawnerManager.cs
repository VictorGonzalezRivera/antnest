﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : MonoBehaviour
{
    [SerializeField] private SpawnerObject spawner;
    [SerializeField] private float spawnRate;
    [SerializeField] private bool instantiateAtStart;
    [SerializeField] private List<Vector3> SpawnPoints;
    [SerializeField] private List<Vector3> SpawnList;
    private float timeNextSpawn;
    private WorldController world;
    public WorldController World { get => world; set => world = value; }
    public float SpawnRate { get => spawnRate; set => spawnRate = value; }

    public int GetEntityCount()
    {
        return spawner.EntityCount;
    }

    void Start()
    {
        SetSpawnPoints();
        timeNextSpawn = spawnRate;
        if (instantiateAtStart)
        {
            for (int i = 0; i < spawner.MaxEntities; i++)
            {
                Vector3 pos = GetSpawnPoint();
                spawner.Spawn(transform, pos);
            }
        }       
    }

    void Update()
    {
        //UpdateSpawnTime();
    }

    public void SelfUpdate()
    {
        UpdateSpawnTime();
    }

    public void UpdateSpawnTime()
    {
        timeNextSpawn -= Time.deltaTime;
        if (timeNextSpawn <= 0)
        {
            timeNextSpawn = spawnRate;
            Vector3 pos = GetSpawnPoint();
            spawner.Spawn(transform, pos, world);
        }
    }

    public void SetSpawnPoints()
    {
        if (SpawnPoints.Count <= 0)
        {
            float division = spawner.SpawnRadius / spawner.EntityRadius;
            for (int i = 0; i < division; i++)
            {
                float x = i * spawner.EntityRadius + Random.Range(spawner.EntityRadius / 3, 2*(spawner.EntityRadius / 3)) - spawner.SpawnRadius / 2;
                for (int j = 0; j < division; j++)
                {
                    float z = j * spawner.EntityRadius + Random.Range(spawner.EntityRadius / 3, 2*(spawner.EntityRadius / 3)) - spawner.SpawnRadius / 2;
                    Vector3 pos = new Vector3(x, 0, z);
                    pos += spawner.CenterPosition;
                    SpawnPoints.Add(pos);
                }
            }

            for (int i = 0; i < SpawnPoints.Count - 1; i++)
            {
                Vector3 temp = SpawnPoints[i];
                int randomIndex = Random.Range(i, SpawnPoints.Count);
                SpawnPoints[i] = SpawnPoints[randomIndex];
                SpawnPoints[randomIndex] = temp;
            }
        }
        SpawnList = SpawnPoints;
        
    }

    public Vector3 GetSpawnPoint()
    {
        Vector3 position = new Vector3();
        if (SpawnList.Count > 0)
        {
            position = SpawnList[0];
            SpawnList.RemoveAt(0);
            if (SpawnList.Count <= 0)
            {
                SetSpawnPoints();
            }

        }
        else
        {
            Debug.Log("Spawn not found");
        }
        return position;
    }

    public void SetInactive(int id)
    {
        spawner.SetInactive(id);
    }

    private void OnApplicationQuit()
    {
        spawner.ClearSpawner();
    }

    public void setMaxEntities(int max)
    {
        spawner.MaxEntities = max;
    }

    public int getMaxEntities()
    {
        return spawner.MaxEntities;
    }
    /*
    [SerializeField] private GameObject entity;
    [SerializeField] private int entityCount;
    public int EntityCount { get { return entityCount; } }
    [SerializeField] private int maxEntities;
    [SerializeField] private float spawnRadius;
    [SerializeField] private float spawnRate;

    private float timeNextSpawn;

    [SerializeField] private List<GameObject> activeList;
    [SerializeField] private List<GameObject> inactiveList;


    void Start()
    {
        timeNextSpawn = spawnRate;
        entity.SetActive(false);

        for (int i = 0; i < maxEntities; i++)
        {
            entityCount++;

            GameObject obj = Instantiate(entity, transform.position, Quaternion.identity);
            obj.transform.SetParent(this.transform);
            inactiveList.Add(obj);
        }
    }


    void Update()
    {
        timeNextSpawn -= Time.deltaTime;
        if (timeNextSpawn <= 0)
        {
            timeNextSpawn = spawnRate;
            Vector3 spawnPosition = Random.onUnitSphere;

            spawnPosition = spawnPosition.normalized * spawnRadius;

            spawnPosition.y = 0;

            GameObject obj = inactiveList[0];

            obj.transform.position = spawnPosition;
            obj.SetActive(true);
           
            activeList.Add(obj);
            inactiveList.RemoveAt(0);
         
        }
    }
    public void SetInactive(int id)
    {
        foreach (GameObject obj in activeList)
        {
            if(obj.GetComponent<Spawnable>().Id == id)
            {
                obj.SetActive(false);
                inactiveList.Add(obj);
                activeList.Remove(obj);
                break;
            }
        }   
    }
    */

}
