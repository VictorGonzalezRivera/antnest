﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawnable : MonoBehaviour
{
    [SerializeField] private int id;
    public int Id { get { return id; } }
    //private SpawnerManager spawner;
    [SerializeField] private SpawnerObject spawner;

    void Start()
    {
        //spawner = transform.parent.GetComponent<SpawnerManager>();
        id = spawner.EntityCount;
    }

    public void SetInactive()
    {
        spawner.SetInactive(id);
    }
}
