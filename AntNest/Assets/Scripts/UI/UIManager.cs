﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField] private SettingsManager settingsManager;

    public static UIManager Instance
    {
        get
        {
            return instance;
        }
        set
        {
            if (instance == null)
            {
                instance = value;
            }
        }
    }
    private static UIManager instance = null;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        
    }


    public void B_Pause()
    {
        WorldController.Instance_.Pause();
    }

    public void B_Settings()
    {
        if (settingsManager.isActive)
        {
            settingsManager.Hide();
        } else
        {
            settingsManager.Show();
        }
    }
}
