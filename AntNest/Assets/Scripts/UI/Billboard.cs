﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    [SerializeField] private Transform cameraTransform;
    Quaternion originalRotation;

    //Sets original rotation at start
    void Start()
    {
        cameraTransform = GameObject.Find("Main Camera").transform;
        originalRotation = transform.rotation;
    }

    //Updates rotation to look at the camera
    void Update()
    {
        if(cameraTransform != null)
        {
            transform.rotation = cameraTransform.rotation * originalRotation;
        } else
        {
            cameraTransform = GameObject.Find("Main Camera").transform;
        }      
    }
}
