﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectClicker : MonoBehaviour
{
    [SerializeField] private Window_ObjectPortrait window;
    [SerializeField] private LayerMask layer;
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100.0f, layer))
            {
                if (hit.transform.gameObject.GetComponentInParent<AntBehaviour>() != null)
                {
                    window.Show(hit.transform, hit.transform.gameObject.GetComponentInParent<AntBehaviour>());
                    PrintName(hit.transform.gameObject);
                }
            } else
            {
                window.Hide();
            }
        }        
    }

    private void PrintName(GameObject go)
    {
        print(go.name);
    }

    
}
