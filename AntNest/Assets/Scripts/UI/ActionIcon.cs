﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionIcon : MonoBehaviour
{
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private List<Sprite> iconList;
    [SerializeField] private bool isActive;

    public void Start()
    {
        this.spriteRenderer = GetComponentInParent<SpriteRenderer>();       
    }

    public void Update()
    {
        if(isActive)
            transform.LookAt(Camera.main.transform.position);
    }

    public void Show(int n)
    {
        //Debug.Log("Mostrando icono: " + iconList[n]);
        spriteRenderer.sprite = iconList[n];
        //Mira hacia la cámarass
        transform.LookAt(Camera.main.transform.position);
        this.gameObject.SetActive(true);
        isActive = true;
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
        isActive = false;
    }
}
