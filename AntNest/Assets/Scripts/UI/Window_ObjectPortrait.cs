﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Window_ObjectPortrait : MonoBehaviour
{
    [SerializeField] private bool active;
    private Transform cameraTransform;
    private Transform followTransform;
    [SerializeField] private AntBehaviour ant;
    
    [SerializeField] private TextMeshProUGUI nameText;
    [SerializeField] private TextMeshProUGUI infoValue1;
    [SerializeField] private TextMeshProUGUI infoValue2;
    [SerializeField] private TextMeshProUGUI infoValue3;
    

    [Range(0.5f, 2.0f)]
    [SerializeField] private float cameraDistance = 0.5f;


    private void Awake()
    {

        cameraTransform = transform.Find("Camera");
        Hide();
    }

    private void Update()
    {
        if (active)
        {
            cameraTransform.position = new Vector3(followTransform.position.x + cameraDistance, followTransform.position.y + cameraDistance, followTransform.position.z);
            UpdateInfo();
        }        
    }

    public void Show(Transform t, AntBehaviour a)
    {
        active = true;
        gameObject.SetActive(true);
        this.followTransform = t;
        this.ant = a;
        nameText.text = ant.kind.ToString();
        if(nameText.text == "DRONE")
        {
            nameText.text = "COLLECTOR";
        }
        UpdateInfo();     
    }
    public void UpdateInfo()
    {
        int health = (int)ant.health;
        int hunger = (int)ant.hunger;
        string state = ant.stateToString();
        infoValue1.text = health.ToString() + "%";
        infoValue2.text = hunger.ToString() + "%";
        infoValue3.text = state;
    }

    public void Hide()
    {
        active = false;
        gameObject.SetActive(false);
    }
}
