﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour
{

    [Header("References")]
    [SerializeField] private WorldController world;
    [SerializeField] private SpawnerManager foodSpawner;
    [SerializeField] private SpawnerManager enemySpawner;

    [Header ("Sliders")]
    [SerializeField] private Slider SpeedSlider;
    [SerializeField] private Slider AntSpawnSlider;
    [SerializeField] private Slider FoodSpawnSlider;
    [SerializeField] private Slider FoodMaxSlider;
    [SerializeField] private Slider EnemySpawnSlider;
    [SerializeField] private Slider EnemyMaxSlider;

    public bool isActive;


    private void Start()
    {
        isActive = false;

        SpeedSlider.value = world.updateLoop;
        //AntSpawnSlider.value = ;
        FoodSpawnSlider.value = (int)FoodSpawnSlider.maxValue - foodSpawner.SpawnRate;
        FoodMaxSlider.value = foodSpawner.getMaxEntities();
        EnemySpawnSlider.value = (int)EnemySpawnSlider.maxValue - enemySpawner.SpawnRate;
        EnemyMaxSlider.value = enemySpawner.getMaxEntities();

        Hide();
    }

    public void setWorldSpeed()
    {
        Debug.Log("Simulation Speed set to: " + (int)SpeedSlider.value);
        world.updateLoop = (int)SpeedSlider.value;
    }

    public void setAntRate()
    {
        // = (int)AntSpawnSlider.value;
    }

    public void setFoodRate()
    {
        foodSpawner.SpawnRate = Mathf.Max((int)FoodSpawnSlider.maxValue - (int)FoodSpawnSlider.value, 1);
    }

    public void setFoodMax()
    {
        foodSpawner.setMaxEntities((int)FoodMaxSlider.value);
    }

    public void setEnemyRate()
    {
        enemySpawner.SpawnRate = Mathf.Max((int)EnemySpawnSlider.maxValue - (int)EnemySpawnSlider.value, 1);
    }

    public void setEnemyMax()
    {
        enemySpawner.setMaxEntities((int)EnemyMaxSlider.value);
    }

    public void Show()
    {
        gameObject.SetActive(true);
        isActive = true;
    }

    public void Hide()
    {
        gameObject.SetActive(false);
        isActive = false;
    }
}
