﻿
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum FSMGlobal
{
    OUTSIDE,
    INSIDE
}
public enum FSMInside
{
    EGG_MODE,
    CARRIED_EGG,
    WALKING,
    BACK,
    BRINGING_FOOD,
    DIGGING,
    WAITING
}

public enum FSMOutside
{
    WALKING,
    BRINGING_FOOD,
    ATTACK,
    RUN_AWAY, 
    SEARCHING,
    BACK,
    WAITING
}



public class AntBehaviour : MonoBehaviour
{

    #region statics
    public static float minStep = 0.2f, maxStep = 0.5f;
    #endregion

    [SerializeField] GameObject FoodFeromonePrefab;
    [SerializeField] GameObject DangerFeromonePrefab;
    [SerializeField] Material soldierMat;
    [SerializeField] Material nurseMat;

    [SerializeField] public FSMGlobal globalState;
    [SerializeField] public FSMInside insideState;
    [SerializeField] public FSMOutside outsideState;
    

    public AntKind kind;

    //El destino hacia el que se mueve actualmente
    Vector3 target;
    Vector3 latestTarget;

    //SOLO PARA INSIDESTATE
    public Stack<Room> roomPath = new Stack<Room>();
    public RoomType diggingRoomType;
    bool goingToDig = false;

    //Comida cercana
    GameObject nearestFood;
    bool carryingFood = false;

    //Enemigo cercano
    EnemyBehaviour nearestEnemy;

    //Feromona de comida cercana
    Feromone nearestFoodFeromone;
    LinkedList<Feromone> visitedFoodFeromones = new LinkedList<Feromone>();

    //Feromona de peligro cercana
    Feromone nearestDangerFeromone;
    LinkedList<Feromone> visitedDangerFeromones = new LinkedList<Feromone>();

    //Cuenta el tiempo entre feromonas
    double feromoneTime = 0;

    //indice de feromonas colocadas (Para controlar el sentido en el que van)
    int feromoneIndex = 0;

    //Parámetros del movimiento
    public float speed;

    //Datos vitales
    public Nest myNest;
    public int id;
    public float age;
    public float hunger;
    public float health;

    public float eggTime;

    float maxAge = 180;

    bool emergencyMode = false;
    public bool visible = true;

    //Huevo
    [SerializeField] public AntBehaviour egg;

    //Primera reina del nido
    bool firstQueen = false;

    //Arbol de decisión
    BehaviourTree behaviourTree;

    //Icono de acción
    [SerializeField] private ActionIcon actionIcon;

    // Start is called before the first frame update
    void Start()
    {
        speed = 1f;
        hunger = 100.0f;
        health = 100.0f;
        age = 0;

        target = transform.position;
        behaviourTree = AntsBehaviourManager.Instance.GetBehaviourTree(this);
        if (kind == AntKind.SOLDIER)
        {
            transform.GetChild(0).GetComponent<MeshRenderer>().material = soldierMat;
            transform.localScale *= 1.5f;
        }
        if (kind == AntKind.NURSE)
        {
            transform.GetChild(0).GetComponent<MeshRenderer>().material = nurseMat;
        }
    }

    public void SelfUpdate()
    {
        if (insideState != FSMInside.EGG_MODE || insideState != FSMInside.CARRIED_EGG)
        {
            updateLife();
        }
     
        if (health > 0)
        {
            switch (globalState)
            {
                case FSMGlobal.INSIDE:
                    switch (insideState)
                    {
                        case FSMInside.EGG_MODE:
                            EclosionBehaviour();
                            break;
                        case FSMInside.CARRIED_EGG:
                            // :)
                            break;
                        case FSMInside.WALKING:
                            moveTowardsTarget();
                            break;
                        case FSMInside.BRINGING_FOOD:
                            moveTowardsTarget();
                            leaveFeromone(FeromoneType.FOOD);
                            break;
                        case FSMInside.DIGGING:
                            digNewRoom();
                            break;
                        case FSMInside.WAITING:
                            behaviourTree.DoTree(this);
                            break;
                    }
                    break;
                case FSMGlobal.OUTSIDE:
                    switch (outsideState)
                    {
                        case FSMOutside.WALKING:
                            moveTowardsTarget();
                            break;
                        case FSMOutside.BRINGING_FOOD:
                            leaveFeromone(FeromoneType.FOOD);
                            moveTowardsTarget();
                            break;
                        case FSMOutside.RUN_AWAY:
                            leaveFeromone(FeromoneType.DANGER);
                            runAwayFromEnemy();
                            break;
                        case FSMOutside.ATTACK:
                            attack();
                            break;
                        case FSMOutside.SEARCHING:
                            break;
                        case FSMOutside.BACK:
                            break;
                        case FSMOutside.WAITING:
                            behaviourTree.DoTree(this);
                            break;
                    }
                    break;
            }
        }                       
    }


    public void EclosionBehaviour()
    {
        
        eggTime -= Time.deltaTime;
        if (eggTime <= 0.0f)
        {
          
            this.transform.GetChild(0).gameObject.SetActive(true);
            this.transform.GetChild(1).gameObject.SetActive(false);
            this.gameObject.name = "ant_" + myNest.id + "_" + this.id + "_" + kind.ToString();
            insideState = FSMInside.WAITING;
            myNest.eggsInBirthRoom = new Queue<AntBehaviour>(myNest.eggsInBirthRoom.Where(x => x != this));

            Debug.Log("hormiga " + this.transform.name + " nace con vida: "+ health  + " y hambre: " + hunger);
        }
    }

    public bool isOutside()
    {
        return globalState == FSMGlobal.OUTSIDE;
    }
    public bool isHungry()
    {
        if(!emergencyMode)
            return hunger < 1;
        return false;
    }
    public bool isCarryingFood()
    {
        return carryingFood;
    }
    public bool isFoodNearby()
    {
        return nearestFood != null;
    }
    public bool isEnemiesNearby()
    {
        return nearestEnemy != null;
    }
    public bool isFoodFeromoneNearby()
    {
        return nearestFoodFeromone != null;
    }   
    public bool isDangerFeromoneNearby()
    {
        return nearestDangerFeromone != null;
    }
    public bool isAtRoomType(RoomType type)
    {
        return roomPath.Count > 0 && roomPath.Peek().Type == type;
    }
    public bool isGoingToDig() {
        return goingToDig;
    }


    //Pone el target a la comida
    public void goToFood()
    {
        outsideState = FSMOutside.WALKING;
        target = nearestFood.transform.position;
    }

    //Pone el target a la feromona
    public void goToFoodFeromone()
    {
        //Debug.Log("go to feromone");
        outsideState = FSMOutside.WALKING;
        target = nearestFoodFeromone.transform.position;
    }

    //Pone el target a la feromona de peligro
    public void goToDangerFeromone()
    {
        //Debug.Log("go to feromone");
        outsideState = FSMOutside.WALKING;
        target = nearestDangerFeromone.transform.position;
    }


    //Ponemos el target a una posición cercana aleatoria
    public void moveRandomly()
    {
        outsideState = FSMOutside.WALKING;

        float angle = Random.Range(-60, 60);
        transform.Rotate(new Vector3(0, 1, 0), angle);
        target = transform.position + transform.forward * Random.Range(minStep, maxStep);

    }

    public void moveTowardsTarget()
    {
        //Si target está fuera de los límites del mundo se reajusta
        target.x = Mathf.Min(Mathf.Max(target.x, -WorldController.Instance_.game_settings.worldLimit), WorldController.Instance_.game_settings.worldLimit);
        target.z = Mathf.Min(Mathf.Max(target.z, -WorldController.Instance_.game_settings.worldLimit), WorldController.Instance_.game_settings.worldLimit);
        target.y = Mathf.Min(target.y, 2);

        //Si hemos llegado al target
        if (isAlmostEqual(transform.position, target))
        {
            //En caso de estar fuera
            if (globalState == FSMGlobal.OUTSIDE)
            {
                //Si estamos en el hormiguero, entramos
                if(myNest != null && isAlmostEqual(target, myNest.transform.position))
                {
                    globalState = FSMGlobal.INSIDE;
                    insideState = FSMInside.WAITING;

                    if(!visible)
                        setVisible(false);
                }   

                //Si no pues nada
                outsideState = FSMOutside.WAITING;
            }
            //En caso de estar dentro:
            else
            {
                setNextTargetRoom();
            }
        }
        else
        {
            //Movemos la posición hacia el target
            Vector3 dir = (target - transform.position).normalized;

            transform.position += dir * speed * Time.deltaTime;

            if ((target - transform.position).normalized != dir)
            {
                transform.position = target;
            }
            else
            {
                transform.forward = (target - transform.position).normalized;
            }
        }
    }

    public void goToNest()
    {
        target = myNest.transform.position;

        if (isCarryingFood())
        {
            outsideState = FSMOutside.BRINGING_FOOD;
        }
        else
        {
            outsideState = FSMOutside.WALKING;
        }
    }

    private void runAwayFromEnemy()
    {
        //Movemos en dirección contraria al enemigo
        if (nearestEnemy != null)
        {
            Vector3 dir = (transform.position - nearestEnemy.transform.position).normalized;
            dir.y = 0;
            transform.forward = (nearestEnemy.transform.position - transform.position).normalized;

            transform.position += dir * speed * Time.deltaTime;
        }
        else
        {
            outsideState = FSMOutside.WAITING;
        }
    }

    private void attack()
    {
        if (nearestEnemy == null || !nearestEnemy.isAlive())
        {
            //Esconder icono de ataque
            actionIcon.Hide();
            outsideState = FSMOutside.WAITING;
        }
        else
        {
            Vector3 enemyPos = nearestEnemy.transform.position;
            //Mostrar icono de ataque
            if(visible)
                actionIcon.Show(1);
            //Si hemos llegado al enemigo
            if (isAlmostEqual(transform.position, enemyPos))
            {
                nearestEnemy.health -= Time.deltaTime * 60;
            }
            else
            {
                //Movemos la posición hacia el enemigo
                Vector3 dir = (enemyPos - transform.position).normalized;

                transform.position += dir * speed * 2 * Time.deltaTime;

                if ((enemyPos - transform.position).normalized != dir)
                    transform.position = nearestEnemy.transform.position;
                else
                    transform.forward = (enemyPos - transform.position).normalized;
            }
        }
    }

    //busca el camino a la nueva room desde la que se encuentra
    //AHORA SIEMPRE SE USA ESTA PARA BUSCAR UNA ROOM
    //Parámetros nuevos:
    //nonEmpty indica si quieres que la room NO pueda estar vacía
    //nonFull indica si quieres que la noom NO pueda estar llena
    public bool goToRoomType(RoomType type, bool nonEmpty, bool nonFull)
    {

        Room targetRoom = myNest.rootRoom.getRoomType(type, nonEmpty, nonFull);
        if (targetRoom != null)
        {
            if (roomPath == null)
            {
                roomPath = new Stack<Room>();
            }
            if (roomPath.Count < 1)
            {
                roomPath.Push(myNest.rootRoom);
            }

            if (isCarryingFood())
            {
                insideState = FSMInside.BRINGING_FOOD;
            }
            else
            {
                insideState = FSMInside.WALKING;
            }
            globalState = FSMGlobal.INSIDE;

            roomPath = myNest.rootRoom.searchPathBetween(roomPath.Peek(), targetRoom);
            target = roomPath.Peek().transform.position;
            return true;
        }
        return false;
    }

    //Se llama sola
    public void setNextTargetRoom()
    {
        //Si queda alguna habitación por visitar, la ponemos al target
        if (roomPath.Count > 1)
        {
            roomPath.Pop();
            target = roomPath.Peek().transform.position;
        }
        //Si no, volvemos a esperar
        else
        {
            //Si hemos llegado al nodo raíz, salimos del hormiguero
            if(roomPath.Peek() == myNest.rootRoom)
            {
                transform.forward = new Vector3(1.0f, 0.0f, 0.0f);
                globalState = FSMGlobal.OUTSIDE;
                if(!visible)
                    setVisible(true);
                outsideState = FSMOutside.WAITING;
            }
            insideState = FSMInside.WAITING;
        }
    }

    //Va hacia la raíz del hormiguero
    public void goOutside()
    {
        //Ya hemos estado en el hormiguero antes
        if (roomPath != null && roomPath.Count > 0)
        {
            //buscamos una pila de habitaciones que lleva a la salida
            roomPath = Room.findWayOut(roomPath.Peek(), new Stack<Room>());
            target = roomPath.Peek().transform.position;
            insideState = FSMInside.WALKING;
        }
        //No hemos estado en el hormiguero antes (puede pasar)
        else
        {
            globalState = FSMGlobal.OUTSIDE;
            outsideState = FSMOutside.WAITING;
        }
        
    }

    public void setEmergencyMode()
    {
        emergencyMode = true;
    }

    public bool leaveFood()
    {
        bool success = roomPath.Peek().addContent(2);
        if (success)
        {
            carryingFood = false;
            //Esconder icono de transportar comida
            actionIcon.Hide();
        }
        insideState = FSMInside.WAITING;
        return success;
    }

    public bool takeFood()
    {
        bool success = roomPath.Peek().takeContent(1);
        if (success)
        {
            hunger = 100;
        }
        insideState = FSMInside.WAITING;
        //Debug.Log("Comida cogida. Exito: " + success);
        return success;
    }

    //Recoge comida de la despensa para llevarla a la reina (Solo NURSE)
    public bool pickUpFood()
    {
        bool success = roomPath.Peek().takeContent(1);
        if (success)
        {
            carryingFood = true;
            //Mostrar Icono de transportar comida
            if(visible)
                actionIcon.Show(0);
            insideState = FSMInside.BRINGING_FOOD;
            Room birthRoom = myNest.rootRoom.getRoomType(RoomType.BIRTH, false, false);
            roomPath = myNest.rootRoom.searchPathBetween(roomPath.Peek(), birthRoom);
        }
        insideState = FSMInside.WAITING;
        return success;
    }
    public void feedQueen()
    {
        AntBehaviour queen = myNest.getQueen();
        if (queen)
            queen.hunger += 25;
        carryingFood = false;
        //Esconder Icono de transportar comida
        actionIcon.Hide();
        insideState = FSMInside.WAITING;
    }

    //Pone a la hormiga a cavar una nueva habitación
    public void startDiggingNewRoom()
    {
        insideState = FSMInside.DIGGING;
        goingToDig = false;
    }

    private void digNewRoom()
    {
        //Miramos si ya tiene una nueva habitación
        Room newRoom = roomPath.Peek().getChildType(diggingRoomType);
        if (newRoom != null)
        {
            Room r = roomPath.Peek();
            roomPath.Clear();
            roomPath.Push(newRoom);
            roomPath.Push(r);
            insideState = FSMInside.WALKING;
        }
        //Si no, seguimos cavando
        else
        {
            roomPath.Peek().digNewRoom(diggingRoomType, Time.deltaTime / 5);
        }
    }

    public void goToRoomToDig(RoomType type)
    {
        if (roomPath.Count <= 0) roomPath.Push(myNest.rootRoom);
        diggingRoomType = type;
        roomPath = myNest.rootRoom.searchPathBetween(roomPath.Peek(), myNest.rootRoom.getDiggingRoom());
        insideState = FSMInside.WALKING;
        goingToDig = true;
    }

    public void leaveFeromone(FeromoneType type)
    {
        if (globalState == FSMGlobal.OUTSIDE)
        {
            //cada 1 segundos
            if (feromoneTime >= 1)
            {
                //TODO:: Utilizar spawner
                Feromone f = null;
                if (type == FeromoneType.FOOD)
                {
                    f = (Instantiate(FoodFeromonePrefab, this.transform.position, Quaternion.identity) as GameObject).GetComponent<Feromone>();
                }
                else
                {
                    f = (Instantiate(DangerFeromonePrefab, this.transform.position, Quaternion.identity) as GameObject).GetComponent<Feromone>();
                }
                f.direction = -transform.forward * 3;
                f.ant_id = id;
                f.index = feromoneIndex++;
                f.inside = globalState == FSMGlobal.INSIDE;
                feromoneTime = 0;

                /*
                foodFeromoneSpawner.SetFeromone(id, -transform.forward * 3, feromoneIndex++, (globalState == FSMGlobal.INSIDE));
                foodFeromoneSpawner.SetSpawnPosition(transform.position);
                foodFeromoneSpawner.Spawn(myNest.transform);
                */
            }
            feromoneTime += Time.deltaTime;
        }
    }
   
    void updateLife()
    {
        int updateLoops = WorldController.Instance_.updateLoop;

        age += Time.deltaTime/updateLoops;
        if (kind != AntKind.QUEEN && age >= maxAge)
            health -= Time.deltaTime / updateLoops;

        if (hunger > 0)
            hunger -= Time.deltaTime / updateLoops;

        if (hunger <= 0.0f)
            health -= Time.deltaTime / updateLoops;

        if (health <= 0.0f)
            die();

    }

    public void die()
    {
        //No deberia estar fuera de estas listas ninguna hormiga
        if (myNest != null)
        {
            myNest.killAnt(this);
        }
        else
        {
            WorldController.Instance_.noNestAnts.Remove(this);
            Destroy(this.gameObject);
        }
    }

    public bool isAlive()
    {
        return health >= 0.0f;

    }

    private void OnTriggerEnter(Collider other)
    {
        //Solo se aplica a las hormigas que están fuera
        if (globalState == FSMGlobal.OUTSIDE)
        {
            //Comida
            if (nearestFood == null && other.tag == "Food")
            {
                nearestFood = other.gameObject;
            }
            //Feromonas de comida
            else if (other.tag == "FoodFeromone")
            {
                //Si la nueva feromona está más cerca que la anterior, la guardamos
                if (nearestFoodFeromone == null ||
                    Vector3.Distance(nearestFoodFeromone.transform.position, transform.position) > Vector3.Distance(other.transform.position, transform.position))
                {
                    //Comprobamos que va en el sentido correcto (hacia la comida)
                    if (isNextFeromone(other.gameObject))
                    {
                        nearestFoodFeromone = other.gameObject.GetComponent<Feromone>();
                        visitedFoodFeromones.AddLast(nearestFoodFeromone);
                        //La hormiga recuerda un máximo de 5 feromonas visitadas
                        if (visitedFoodFeromones.Count > 5)
                        {
                            visitedFoodFeromones.RemoveFirst();
                        }
                    }
                }

            }
            else if(other.tag == "DangerFeromone")
            {
                if(kind != AntKind.SOLDIER)
                    moveTo(other.transform.position - transform.position * 3);
                else if(outsideState != FSMOutside.ATTACK)
                {
                    //Si la nueva feromona está más cerca que la anterior, la guardamos
                    if (nearestDangerFeromone == null ||
                        Vector3.Distance(nearestDangerFeromone.transform.position, transform.position) > Vector3.Distance(other.transform.position, transform.position))
                    {
                        //Comprobamos que va en el sentido correcto (hacia el peligro)
                        if (isNextFeromone(other.gameObject))
                        {
                            nearestDangerFeromone = other.gameObject.GetComponent<Feromone>();
                            visitedDangerFeromones.AddLast(nearestDangerFeromone);
                            //La hormiga recuerda un máximo de 5 feromonas visitadas
                            if (visitedDangerFeromones.Count > 5)
                            {
                                visitedDangerFeromones.RemoveFirst();
                            }
                        }
                    }
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //Solo se aplica a las hormigas que están fuera
        if (globalState == FSMGlobal.OUTSIDE)
        {
            if (other.gameObject == nearestFood)
            {
                nearestFood = null;
            }
            else if ( nearestEnemy != null && other.gameObject == nearestEnemy.gameObject)
            {
                outsideState = FSMOutside.WAITING;
                nearestEnemy = null;
            }
            else if (other.gameObject == nearestFoodFeromone)
            {
                nearestFoodFeromone = null;
            }
            else if (other.gameObject == nearestDangerFeromone)
            {
                nearestDangerFeromone = null;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        //Solo se aplica a las hormigas que están fuera
        if (globalState == FSMGlobal.OUTSIDE)
        {
            if (isFoodNearby())
            {
                if (!nearestFood.activeSelf)
                {
                    nearestFood = null;
                    outsideState = FSMOutside.WAITING;
                }
                //Al llegar a la comida, ésta se destruye
                else if (other.gameObject == nearestFood && isAlmostEqual(other.transform.position, transform.position))
                {
                    nearestFood.GetComponent<Food>().takePiece();
                    nearestFood = null;
                    if (emergencyMode)
                    {
                        hunger = 100;
                        emergencyMode = false;
                    }
                    else
                    {
                        carryingFood = true;
                        //Mostrar icono de transportar comida
                        if (visible)
                            actionIcon.Show(0);
                        feromoneTime = 0;
                    }
                }
            }
            if (isFoodFeromoneNearby())
            {
                //Al llegar a la feromona, pasamos a estado de espera
                if ((nearestFoodFeromone != null && isAlmostEqual(nearestFoodFeromone.transform.position, transform.position)) || 
                    (nearestDangerFeromone != null && isAlmostEqual(nearestDangerFeromone.transform.position, transform.position)))
                {
                    moveTo(nearestFoodFeromone.GetComponent<Feromone>().direction);
                    nearestFoodFeromone = null;
                    outsideState = FSMOutside.WAITING;
                }
            }

            //Enemigos
            else if (other.tag == "Enemy")
            {
                EnemyBehaviour newEnemy = other.GetComponent<EnemyBehaviour>();
                if((nearestEnemy == null && nearestEnemy != newEnemy) || Vector3.Distance(newEnemy.transform.position, transform .position) < Vector3.Distance(nearestEnemy.transform.position, transform.position))
                    nearestEnemy = newEnemy;

                if (kind == AntKind.SOLDIER)
                    outsideState = FSMOutside.ATTACK;
                else
                    outsideState = FSMOutside.RUN_AWAY;
            }
            
            //if (outsideState == FSMOutside.BRINGING_FOOD && isAlmostEqual(transform.position, target))
            //{
            //    globalState = FSMGlobal.INSIDE;
            //    insideState = FSMInside.WAITING;
            //}
        }

    }

    private bool isNextFeromone(GameObject obj)
    {
        Feromone newFeromone = obj.GetComponent<Feromone>();

        if(newFeromone != null && !visitedFoodFeromones.Contains(newFeromone) && 
            newFeromone.inside == (globalState == FSMGlobal.INSIDE))
        {
            //¿Hemos visitado más feromonas antes?
            if (visitedFoodFeromones.Count > 0)
            {
                //la feromona anterior sigue activa
                if (visitedFoodFeromones.Last.Value != null)
                {
                    //La feromona es de la misma hormiga que la anterior
                    //La feromona tiene un indice menor (va en el sentido correcto)
                    Feromone lastFeromone = visitedFoodFeromones.Last.Value;
                    return lastFeromone.index > newFeromone.index && lastFeromone.ant_id == newFeromone.ant_id;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }

    private void moveTo(Vector3 direction)
    {
        target = transform.position + direction;
        //transform.forward = target - transform.position;
        outsideState = FSMOutside.WALKING;
    }

    private bool isAlmostEqual(Vector3 v1, Vector3 v2)
    {
        return Vector3.Distance(v1, v2) <= 0.01;
    }

    public string stateToString()
    {
        string state = "";
        if (globalState == FSMGlobal.INSIDE)
        {
            state = insideState.ToString();
        } else
        {
            state = outsideState.ToString();
        }
        return state;
    }

    //No pone el atributo visible, porque se puede visible teniendo visible = false cuando se está fuera
    public void setVisible(bool visible)
    {
        foreach (MeshRenderer rend in GetComponentsInChildren<MeshRenderer>(true))
            rend.enabled = visible;
        if (!visible) { actionIcon.Hide(); }
    }

    #region Metodos de la hormiga reina

    public void RunFromEnemy()
    {
        Vector3 dir = this.transform.position - nearestEnemy.transform.position;
        transform.forward = -dir;
        target = dir.normalized * maxStep;
        moveTowardsTarget();
    }

    public bool IsGoodPlaceForNewNest()
    {
        //La condicion es que este alejada de cualquier nido N unidades
        float N = WorldController.Instance_.game_settings.nestsDistance;

        foreach (Nest nest in WorldController.Instance_.activeNests)
        {
            float distance = Vector3.Distance(transform.position, nest.transform.position);
            if (distance < N)
            {
                return false;
            }
        }
        return true;
    }

    public bool DoIHaveNest()
    {
        return !(myNest == null);
    }

    public void CreateNewNest()
    {
        Vector3 nestPosOffset = Vector3.zero;
        Nest newNest = WorldController.Instance_.CreateNewNest(this.transform.position);
        if(newNest)
        {
            newNest.AddAntToNest(this);
            myNest = newNest;
            firstQueen = true;
            myNest.firstQueenPointer = this;
            Debug.Log("nido " + myNest.id + " creado en (" + myNest.transform.position.x + ", " + myNest.transform.position.z + ")");
        }

        // Si el nest es invisible, me hago invisible (Un poco guarro pero es lo que hay)
        if ( newNest != null && !newNest.isVisible)
        {
            foreach (MeshRenderer rend in GetComponentsInChildren<MeshRenderer>(true))
                rend.enabled = false;
        }

    }

    public void LookForNewNestPlace()
    {
        moveRandomly();
        /*
        Vector3 dir = (transform.position - myNest.transform.position).normalized;
        if(dir.magnitude > 0.0001f) //Margen de error  en vez de comparar a 0
        {
            Vector3 targetNew = this.transform.position + dir * Random.Range(minStep, maxStep);
            target = targetNew;
            moveTowardsTarget();
        }
        */
    }

    public bool BirthRoomExist()
    {
        Room r = myNest.rootRoom;
        return !(r.getRoomType(RoomType.BIRTH, false, false) == null);
    }


    float birthTime = 5.0f;
    float currentBirthTime = 0.0f;
    public void GiveBirth()
    {
        currentBirthTime += Time.deltaTime;

        if (currentBirthTime >= birthTime)
        {
            if (myNest.activeAnts.Count < WorldController.Instance_.game_settings.maxAntsPerNest)
            {
                //Decidir a que da a luz segun sus probabilidades
                AntKind kind = BirthKind();
                //Dar a luz 
                myNest.GiveBirthTo(kind, this);
                //Debug.Log("Nace una nueva hormiga del tipo: " + kind.ToString());
                //Reajustar probabilidades
                ReadjustProbsFor(kind);
            }
            currentBirthTime -= birthTime;
        }
    }


    private AntKind BirthKind()
    {
        float[] spawnProbsSorted = new float[myNest.spawnProbs.Length];

        int[] converter = new int[myNest.spawnProbs.Length];

        for (int i = 0; i < myNest.spawnProbs.Length; i++)
        {
            spawnProbsSorted[i] = myNest.spawnProbs[i];
            converter[i] = i;
        }

        //Reordenar de menor a mayor y guardar indices
        for (int i = 0; i < spawnProbsSorted.Length - 1; i++)
        {
            for (int j = i + 1; j < spawnProbsSorted.Length; j++)
            {
                if (spawnProbsSorted[i] > spawnProbsSorted[j])
                {
                    //Reordenar
                    float aux = spawnProbsSorted[i];
                    spawnProbsSorted[i] = spawnProbsSorted[j];
                    spawnProbsSorted[j] = aux;

                    //Guardo
                    int index_aux = converter[i];
                    converter[i] = converter[j];
                    converter[j] = index_aux;

                }
            }
        }

        //Probabilidades ordenadas acumuladas
        float[] stackedProbs = new float[spawnProbsSorted.Length + 1];
        stackedProbs[0] = 0;
        stackedProbs[1] = spawnProbsSorted[0];

        for (int i = 2; i < stackedProbs.Length; i++)
        {
            stackedProbs[i] = (stackedProbs[i - 1] + spawnProbsSorted[i - 1]);
        }

        //Random para buscar por probabilidad
        float max = stackedProbs[stackedProbs.Length - 1];

        float value = Random.Range(0, max);
        int index_resultado = -1;
        for (int i = 0; i < stackedProbs.Length - 1; i++)
        {
            if (value > stackedProbs[i] && value < stackedProbs[i + 1])
            {
                index_resultado = i + 1;
                break;
            }
        }

        //Evaluamos el index en el array de indices y sacamos el tipo de hormiga
        return (AntKind)(converter[index_resultado - 1]);
    }

    private void ReadjustProbsFor(AntKind kind)
    {
        float dec_multiplier = myNest.spawnDecreasingMult[(int)kind];
        float currentProb = myNest.spawnProbs[(int)kind];
        //Asegurar por lo menos una minima probabilidad
        myNest.spawnProbs[(int)kind] = Mathf.Max(WorldController.Instance_.game_settings.minSpawnProb, dec_multiplier * currentProb);
    }

    public bool isFirstQueen()
    {
        return firstQueen;
    }

    public void LeaveNest()
    {

        if (globalState == FSMGlobal.INSIDE)
        {
            Debug.Log("dejar hormiguero dentro (LEAVE_NEST)");
            goOutside();
        }
        else if (globalState == FSMGlobal.OUTSIDE && myNest != null)
        {
            Debug.Log("dejar hormiguero fuera (LEAVE_NEST)");
            myNest.RemoveAntFromNest(this);
            WorldController.Instance_.noNestAnts.Add(this);
            myNest = null;
            roomPath = new Stack<Room>();
        }
       
    }

    #endregion Metodos de la Reina hormiga

    #region Metodos de Nurses
    public bool IsBirthRoomEmpty()
    {
        Room birthRoom = myNest.rootRoom.getRoomType(RoomType.BIRTH, false, false);
        return birthRoom.Content <= 0;
    }

    public bool isCarryingEgg()
    {
        return !(egg == null);
    }

    public void DeployEgg()
    {
        Room currentRoom = roomPath.Peek();
        if (egg == null)
        {
            Debug.Log("NO hay huevo que dejar");
        }
        else
        {
            egg.roomPath = new Stack<Room>(this.roomPath); //Asigno roompath
            egg.transform.parent = null; //parent seria world teoricamente
            egg.insideState = FSMInside.EGG_MODE;
            Debug.Log("Se deja el huevo con nombre: " + egg.transform.name);
            egg = null;
            //Esconder icono de huevo
            actionIcon.Hide();
        }
        insideState = FSMInside.WAITING;
    }

    public bool PickUpEgg()
    {
        Room currentRoom = roomPath.Peek();
        if (currentRoom.Type != RoomType.BIRTH)
        {
            Debug.Log("Estas pillando un huevo fuera de la habitacion de la reina");
        }
        if (myNest.eggsInBirthRoom.Count <= 0)
        {
            return false;
        }

        AntBehaviour egg_ = myNest.eggsInBirthRoom.Dequeue();
        if (egg == null)
        {
            egg = egg_;
            egg.transform.parent = this.transform;
              egg.insideState = FSMInside.CARRIED_EGG;
            //Mostrar icono de Huevo
            if (visible)
                actionIcon.Show(2);
        }
        else
        {
            Debug.Log(this.gameObject.name + "ya lleva un huevo");
        }
        return true;

    }

    public bool IsThereEggs()
    {
        return myNest.eggsInBirthRoom.Count > 0;
    }


    #endregion Metodos de Nurses
}
