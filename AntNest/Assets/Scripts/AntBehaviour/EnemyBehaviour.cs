﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FSMEnemy
{
    WALKING,
    ATTACKING,
    WAITING
}

public class EnemyBehaviour : MonoBehaviour
{

    public WorldController wController;

    static float minStep = 0.5f, maxStep = 1.0f;

    Vector3 target;
    Vector3 latestTarget;

    AntBehaviour prey;

    FSMEnemy state;

    // Datos vitales
    float age = 0f;
    public float health = 100;
    public float speed = 2.0f;

    float maxAge = 180;

    //Árbol de decisión
    BehaviourTree behaviourTree;

    // Start is called before the first frame update
    void Start()
    {
        target = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SelfUpdate()
    {
        if (gameObject.activeSelf)
        {
            updateLife();
            if (health > 0)
            {
                switch (state)
                {
                    case FSMEnemy.WALKING:
                        moveTowardsTarget();
                        break;
                    case FSMEnemy.WAITING:
                        moveRandomly();
                        break;
                    case FSMEnemy.ATTACKING:
                        chaseAnt();
                        break;
                }
            }
        }      
    }

    public bool isAlive()
    {
        return health >= 0 && gameObject.activeSelf;
    }

    private void updateLife()
    {
        age += Time.deltaTime;

        if(age >= maxAge)
            health -= Time.deltaTime;

        if(health <= 0)
        {
            /*
            wController.enemies.Remove(this);
            Destroy(this.gameObject);
            */
            age = 0;
            health = 100;
            gameObject.GetComponent<Spawnable>().SetInactive();
        }
    }

    private void moveTowardsTarget()
    {
        //Si target está fuera de los límites del mundo se reajusta
        target.x = Mathf.Min(Mathf.Max(target.x, -WorldController.Instance_.game_settings.worldLimit), WorldController.Instance_.game_settings.worldLimit);
        target.z = Mathf.Min(Mathf.Max(target.z, -WorldController.Instance_.game_settings.worldLimit), WorldController.Instance_.game_settings.worldLimit);
        

        //Si hemos llegado al target
        if ((transform.position - target).magnitude < 0.001f)
        {
            state = FSMEnemy.WAITING;
        }
        else
        {
            //Movemos la posición hacia el target
            Vector3 dir = (target - transform.position).normalized;

            transform.position += dir * speed * Time.deltaTime;

            if ((target - transform.position).normalized != dir)
            {
                transform.position = target;
            }
            else
            {
                transform.forward = (target - transform.position).normalized;
            }
        }

       
    }

    private void chaseAnt()
    {
        if (prey.globalState == FSMGlobal.OUTSIDE)
        {
            if ((transform.position - prey.transform.position).magnitude <= 0.1f)
            {
                prey.health -= Time.deltaTime * 50;
                if (!prey.isAlive())
                {
                    prey.die();
                    target = transform.position;
                    state = FSMEnemy.WAITING;
                }
            }

            //Movemos la posición hacia el target
            Vector3 dir = (prey.transform.position - transform.position).normalized;

            transform.position += dir * speed * Time.deltaTime;
            if ((transform.forward = (prey.transform.position - transform.position).normalized) == -dir)
            {
                transform.position = prey.transform.position;
            }
        }
        else
        {
            prey = null;
            state = FSMEnemy.WAITING;
        }
    }

    private void moveRandomly()
    {
        state = FSMEnemy.WALKING;
        //Angulo random al que va a girarse la hormiga
        float minAngle = 60.0f, maxAngle = 60.0f;
        float desiredAngle = Random.Range(minAngle, maxAngle);

        if (Random.Range(0.0f, 1.0f) >= 0.5f) desiredAngle *= -1;

        //Que rotacion tiene forward con respecto al eje X
        float radiansDesiredAngle = Mathf.PI * desiredAngle / 180.0f;
        //Si los dos vectores son perpendiculares, Dot da 0 y el Acos da 
        //float forwardRadiansAngle = Mathf.Acos(Vector3.Dot(transform.forward, Vector3.right) * Mathf.PI / 180.0f);

        Vector3 newTarget = Vector3.zero;
        newTarget.x = transform.forward.x * Mathf.Cos(radiansDesiredAngle) - transform.forward.z * Mathf.Sin(radiansDesiredAngle);
        newTarget.z = transform.forward.x * Mathf.Sin(radiansDesiredAngle) + transform.forward.z * Mathf.Cos(radiansDesiredAngle);
        newTarget = newTarget.normalized;

        target = transform.position + newTarget * Random.Range(minStep, maxStep); //* Random.Range(minStep, minStep);        
        
    }

    private void OnTriggerEnter(Collider other)
    {
        //AntBehaviour ant = other.GetComponent<AntBehaviour>();
        //if (ant != null && ant.isOutside())
        //{
        //    if (state == FSMEnemy.ATTACKING)
        //    {
        //        if (target != null && (target - transform.position).magnitude > (other.transform.position - transform.position).magnitude)
        //        {
        //            target = other.transform.position;
        //            prey = ant;
        //        }
        //    }
        //    else
        //    {
        //        target = other.transform.position;
        //        prey = ant;
        //        state = FSMEnemy.ATTACKING;
        //    }
        //}
    }
    private void OnTriggerStay(Collider other)
    {
        AntBehaviour ant = other.GetComponent<AntBehaviour>();
        if (ant != null && ant.isOutside())
        {
            state = FSMEnemy.ATTACKING;

            if (prey == null)
                prey = ant;
            else
            {
                float preyDist = Vector3.Distance(prey.transform.position, transform.position);
                float newAntDist = Vector3.Distance(ant.transform.position, transform.position);
                if (ant == prey || newAntDist < preyDist)
                    prey = ant;
            }
            if (Vector3.Distance(prey.transform.position, transform.position) <= 0.01)
            {
                prey.health -= Time.deltaTime * 10;
            }
        }

        if (prey != null && !prey.isAlive())
        {
            prey = null;
            state = FSMEnemy.WAITING;
        }
    }
}
