﻿using System.Collections.Generic;
using UnityEngine;

public class Nest : MonoBehaviour
{
    static AntBehaviour SpawnAnt(Nest n, AntKind ak)
    {
        return null;
    }

    WorldController world_controller;

    [HideInInspector]
    public int id;
    public int[] nAntsKind = new int[System.Enum.GetNames(typeof(AntKind)).Length];
    public float[] spawnProbs = new float[System.Enum.GetNames(typeof(AntKind)).Length];
    public float[] spawnDecreasingMult = new float[System.Enum.GetNames(typeof(AntKind)).Length];
    public Room roomPrefab;

    public Room rootRoom;

    Queue<AntBehaviour> ants = new Queue<AntBehaviour>();
    public List<AntBehaviour> activeAnts = new List<AntBehaviour>();
    [HideInInspector] public Queue<AntBehaviour> eggsInBirthRoom = new Queue<AntBehaviour>();

    [HideInInspector] public AntBehaviour firstQueenPointer = null; 

    public bool isVisible = false;

    private void OnEnable()
    {
        //Inicializacion de rooms
        rootRoom = GetComponent<Room>();
        rootRoom.children = new List<Room>();
        rootRoom.childAngle = 45;
        rootRoom.createOnlyChildRoom(RoomType.FOOD);
        //

        world_controller = WorldController.Instance_;
        InitAntPool();
        SetInitProbs();
    }


    private void OnDisable()
    {
        ants = new Queue<AntBehaviour>();
        activeAnts = new List<AntBehaviour>();
    }

    void Start()
    {

    }

    public void InitAntPool()
    {
        for (int i = 0; i < world_controller.game_settings.maxAntsPerNest; i++)
        {
            ants.Enqueue(CreateAnt(AntKind.COLLECTOR));
        }
    }

    private void SetInitProbs()
    {
        //SpawnProbs
        spawnProbs[(int)AntKind.QUEEN] = world_controller.game_settings.queen.spawnProb;
        spawnProbs[(int)AntKind.DRONE] = world_controller.game_settings.drone.spawnProb;
        spawnProbs[(int)AntKind.SOLDIER] = world_controller.game_settings.soldier.spawnProb;
        spawnProbs[(int)AntKind.COLLECTOR] = world_controller.game_settings.collector.spawnProb;
        spawnProbs[(int)AntKind.NURSE] = world_controller.game_settings.nurse.spawnProb;
        //Spawn decreasingProbs
        spawnDecreasingMult[(int)AntKind.QUEEN] = world_controller.game_settings.queen.spawnDecreasingMultiplier;
        spawnDecreasingMult[(int)AntKind.DRONE] = world_controller.game_settings.drone.spawnDecreasingMultiplier;
        spawnDecreasingMult[(int)AntKind.SOLDIER] = world_controller.game_settings.soldier.spawnDecreasingMultiplier;
        spawnDecreasingMult[(int)AntKind.COLLECTOR] = world_controller.game_settings.collector.spawnDecreasingMultiplier;
        spawnDecreasingMult[(int)AntKind.NURSE] = world_controller.game_settings.nurse.spawnDecreasingMultiplier;
    }

    private AntBehaviour CreateAnt(AntKind ak)
    {
        GameObject ant = Instantiate<GameObject>(world_controller.game_settings.antPrefab);
        ant.GetComponent<AntBehaviour>().kind = ak;
        ant.gameObject.SetActive(false);
        ant.transform.position = this.transform.position;
        return ant.GetComponent<AntBehaviour>();
    }

    public void AddAntToNest(AntBehaviour newAnt)
    {
        nAntsKind[(int)newAnt.kind]++;
        newAnt.id = nAntsKind[(int)newAnt.kind];
        activeAnts.Add(newAnt);
        newAnt.myNest = this;
        newAnt.transform.name = "ant_" + newAnt.myNest.id + "_" + newAnt.id + "_" + newAnt.kind.ToString();
    }

    public void RemoveAntFromNest(AntBehaviour ant)
    {
        activeAnts.Remove(ant);
        nAntsKind[(int)ant.kind]--;
        ant.transform.name = "ant_" + "NO_NEST_" + "NO_ ID_" + ant.kind.ToString();
    }

    public AntBehaviour GiveBirthTo(AntKind ak, AntBehaviour myMother)
    {
        if (ants.Count > 0)
        {
            AntBehaviour newAnt = ants.Dequeue();
            newAnt.myNest = this;
            newAnt.id = activeAnts.Count;
            newAnt.gameObject.name = "ant_" + id + "_" + newAnt.id + "_" + ak.ToString() + "_EGG";
            newAnt.kind = ak;

            //Valores para que salga el huevo al lado de su madre
            newAnt.transform.position = myMother.transform.position;
            newAnt.globalState = FSMGlobal.INSIDE;
            newAnt.outsideState = FSMOutside.WAITING;
            newAnt.insideState = FSMInside.EGG_MODE;
            GameSettingsSO gs = WorldController.Instance_.game_settings;
            newAnt.eggTime = Random.Range(gs.minEggTime, gs.maxEggTime);
            //Lo meto en la cola de huevos esperando
            eggsInBirthRoom.Enqueue(newAnt);
            //Activo la visual del huevo
            newAnt.transform.GetChild(0).gameObject.SetActive(false);
            newAnt.transform.GetChild(1).gameObject.SetActive(true);
            newAnt.roomPath = new Stack<Room>(myMother.roomPath);
            newAnt.globalState = FSMGlobal.INSIDE;
            newAnt.insideState = FSMInside.EGG_MODE;
            newAnt.visible = isVisible;
            ///////////
            newAnt.gameObject.SetActive(true);

            //Si el nest es invisible, las nuevas hormigas nacen invisibles
            if (!isVisible)
                foreach (MeshRenderer rend in newAnt.GetComponentsInChildren<MeshRenderer>(true))
                    rend.enabled = false;

            activeAnts.Add(newAnt);
            //Podria estar bien dar variabilidad a la vida, hambre, velocidad, etc al nacer
            return newAnt;
        }
        return null;
        
    }

    public void killAnt(AntBehaviour ant)
    {
        activeAnts.Remove(ant);
        ants.Enqueue(ant);
        nAntsKind[(int)ant.kind]--;
        ant.gameObject.SetActive(false);    
    }

    public void SelfUpdate()
    {
        for(int i = activeAnts.Count-1; i >= 0; i--)
        {
            activeAnts[i].SelfUpdate();
        }
        
    }

    public AntBehaviour getQueen()
    {/*
        foreach(AntBehaviour ab in activeAnts)
        {
            if(ab.kind == AntKind.QUEEN && ab.isFirstQueen())
            {
                return ab;
            }
        }
        Debug.LogError("get queen returning null");
        return null;
        */
        return firstQueenPointer;
    }

}
