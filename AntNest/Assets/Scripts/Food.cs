﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{
    [SerializeField] private Vector3 position;

    [SerializeField] private float value;
    [SerializeField] private float expireTime;
    [SerializeField] private float expireTimeLeft;

    float initValue;
    
    private GameObject obj;

    void Start()
    {

        position = transform.position;
        obj = this.gameObject;
        expireTimeLeft = expireTime;
        initValue = value;
    }

    // Update is called once per frame
    void Update()
    {
        /*
        expireTimeLeft -= Time.deltaTime;
        if (expireTimeLeft <= 0)
        {
            SetInactive();
            expireTimeLeft = expireTime;
        }*/
    }

    public void SetInactive()
    {
        obj.GetComponent<Spawnable>().SetInactive();
        value = initValue;
        transform.localScale = new Vector3(1, 1, 1);
    }

    public void takePiece()
    {
        value--;
        updateSize();
    }

    public void updateSize()
    {
        float scale = value / initValue;
        transform.localScale = new Vector3(scale, scale, scale);
        if (value <= 0)
        {
            SetInactive();
        }
        
    }
}
