﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AntProject/GameSettingsSO", fileName = "new GameSettingsSO")]
public class GameSettingsSO : ScriptableObject
{
    [Header("World")]
    public float worldLimit = 15;
    [Range(1, 10)]
    public int nUpdatesLoop;

    [Header("Prefab de ant")]
    public GameObject antPrefab;

    [Header("Ant Hill")]
    public GameObject antHillPrefab;

    [Header("Egg")]
    public GameObject eggModel;

    [Header("Distance Between Nests")]
    [Range(1, 20)] public int nestsDistance;

    [Header("MaxAntsPerNest")]
    [Range(1, 100)]public int maxAntsPerNest;


    [Header("EggTimes")]
    [Range(1.0f, 10.0f)]
    public float minEggTime;
    [Range(10.0f, 40.0f)]
    public float maxEggTime;

    [Header("Minimum spawn prob")]
    [Range(0.1f, 0.5f)]
    public float minSpawnProb = 0.1f;

    [Header("Settings in order of AntKind")]
    public AntSettings queen;
    public AntSettings drone;
    public AntSettings soldier;
    public AntSettings collector;
    public AntSettings nurse;

    



    void Start()
    {
        
    }

}
