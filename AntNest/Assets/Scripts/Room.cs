﻿using System.Collections.Generic;
using UnityEngine;

public enum RoomType
{
    EMPTY,
    FOOD,
    BIRTH,
    EGGS
}

public class Room : MonoBehaviour
{

    public Room roomPrefab;

    [SerializeField] GameObject fillMask;
    [SerializeField] GameObject loadBar;

    Room parent = null;

    public List<Room> children = new List<Room>();

    [SerializeField] int maxCapacity = 10;

    public int maxChildren = 3;

    public int Content { get; private set; }

    public RoomType Type { get; private set; }

    [SerializeField] int baseAngle = 45;

    int depth = 0;

    Dictionary<RoomType, float> diggingPercentage = new Dictionary<RoomType, float>();

    public int childAngle = 30;

    bool isVisible = false;

    // Start is called before the first frame update
    void Start()
    {
        diggingPercentage = new Dictionary<RoomType, float>();
        updateFillMask();
        if (loadBar != null)
            loadBar.transform.parent.gameObject.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public Room(RoomType type)
    {
        this.Type = type;
        //maxChildren = 3;
        //maxCapacity = 3; 
    }

    private void updateFillMask()
    {
        if(fillMask != null && Type == RoomType.FOOD)
            fillMask.transform.localPosition= new Vector3(-4 + ((float)Content / (float)maxCapacity) * 3.5f, 0, 0);
    }

    //Devuelve false si está lleno
    public bool addContent(int amount)
    {
        if (Content >= maxCapacity)
            return false;
        Content += amount;
        updateFillMask();
        return true;
    }

    //Devuelve false si está vacío
    public bool takeContent(int amount)
    {
        if (Content <= 0)
            return false;
        Content -= amount;
        updateFillMask();
        return true;
    }

    public bool isChildrenFull()
    {
        return children.Count >= maxChildren;
    }

    //Requere varias pasadas para crear una nueva habitación hijo
    public bool digNewRoom(RoomType type, float amount)
    {

        if (isChildrenFull())
        {
            return false;
        }
        else
        {
            if (loadBar != null && isVisible)
                loadBar.transform.parent.gameObject.SetActive(true);

            //Si es la primera vez que cavamos este tipo de hijo, lo añadimos
            if (!diggingPercentage.ContainsKey(type))
            {
                if (loadBar != null)
                {
                    loadBar.transform.localScale = new Vector3(1, 1, 0);
                    loadBar.transform.parent.rotation = Quaternion.LookRotation(Vector3.forward, Vector3.up);
                }
                diggingPercentage.Add(type, 0.0f);
            }

            if (diggingPercentage[type] >= 1)
            {
                if (loadBar != null)
                    loadBar.transform.parent.gameObject.SetActive(false);
                createNewChildRoom(type);
                diggingPercentage.Remove(type);
            }
            else
            {
                diggingPercentage[type] += amount;
                if (loadBar != null)
                {
                    loadBar.transform.localScale = new Vector3(1, 1, diggingPercentage[type]);
                    loadBar.transform.parent.gameObject.SetActive(isVisible);
                }

            }
            return true;
        }
    }

    public void createOnlyChildRoom(RoomType type)
    {
        if (children.Count == 0)
        {
            Room child = Instantiate(roomPrefab);


            child.transform.position = this.transform.position;
            child.transform.Translate(0, -5, 0, Space.Self);

            child.transform.rotation = this.transform.rotation;

            child.Type = type;

            child.parent = this;

            child.maxChildren = 3;

            child.depth = depth + 1;

            child.isVisible = isVisible;

            if (!isVisible)
            {
                foreach (MeshRenderer rend in child.GetComponentsInChildren<MeshRenderer>())
                    rend.enabled = false;
                SpriteRenderer sr = child.GetComponentInChildren<SpriteRenderer>(true);
                if (sr != null)
                    sr.enabled = false;
                SpriteMask sm = child.GetComponentInChildren<SpriteMask>(true);
                if (sm != null)
                    sm.enabled = false;
            }

            children.Add(child);

            maxChildren = 1;
        }
    }

    public void createNewChildRoom(RoomType type)
    {
        if (!isChildrenFull())
        {
            Room child = Instantiate(roomPrefab);

            int angleDeg = -childAngle + children.Count * childAngle;
            float angleRad = Mathf.Deg2Rad * angleDeg;

            child.transform.position = this.transform.position;

            float length = 5.0f;

            float sidetranslation = -Mathf.Sin(angleRad) * length;
            float verticaltranslation = -Mathf.Cos(angleRad) * length;
            child.transform.Translate(0, verticaltranslation, sidetranslation, Space.Self);

            child.transform.rotation = this.transform.rotation;
            child.transform.Rotate(angleDeg, 0, 0, Space.Self);

            child.Type = type;

            child.parent = this;

            child.maxChildren = 3;

            child.depth = depth + 1;

            child.childAngle = childAngle - 5 * depth;

            child.isVisible = true;

            if (!isVisible)
            {
                foreach (MeshRenderer rend in child.GetComponentsInChildren<MeshRenderer>())
                    rend.enabled = false;
                SpriteRenderer sr = child.GetComponentInChildren<SpriteRenderer>(true);
                if (sr != null)
                    sr.enabled = false;
                SpriteMask sm = child.GetComponentInChildren<SpriteMask>(true);
                if (sm != null)
                    sm.enabled = false;
            }

            children.Add(child);
        }
    }

    //Devuelve un hijo de un tipo en concreto si hay
    public Room getChildType(RoomType type)
    {
        foreach (Room child in children)
        {
            if (child.Type == type && child.Content < child.maxCapacity)
            {
                return child;
            }
        }
        return null;
    }

    //Devuelve el camino entre dos nodos cualquiera
    public Stack<Room> searchPathBetween(Room roomA, Room roomB)
    {
        //if(roomA == null || roomB == null)
        //{
        //    return null;
        //}
        List<Room> pathA;
        List<Room> pathB;

        //Buscamos el recorrido hasta cada habitación desde la raíz
        pathA = new List<Room>(searchRoom(this, roomA, new Stack<Room>()));
        pathB = new List<Room>(searchRoom(this, roomB, new Stack<Room>()));

        //Buscamos la intersección entre los dos recorridos
        //Lo meto en un scope porque quiero seguir llamando i a los iteradores de los for después (manías)
        int intersection = -1;
        {
            int i = 0;
            while (true)
            {
                if (i >= pathA.Count || i >= pathB.Count)
                {
                    intersection = i - 1;
                    break;
                }
                else if (pathA[i] == pathB[i])
                {
                    i++;
                }

                else
                {
                    intersection = i - 1;
                    break;
                }
            }
        }

        Stack<Room> path = new Stack<Room>();

        if(intersection == -1)
        {
            intersection = pathA.Count - 1;
        }
        //Unimos los dos recorridos por la intersección
        for (int i = pathB.Count - 1; i > intersection; i--)
        {
            path.Push(pathB[i]);
        }
        for (int i = intersection; i < pathA.Count; i++)
        {
            path.Push(pathA[i]);
        }

        return path;

    }

    //Función auxiliar para searchPathBetween()
    //Devuelve el camino entre dos nodos (currentRoom va a ser la raíz)
    public Stack<Room> searchRoom(Room currentRoom, Room targetRoom, Stack<Room> path)
    {
        foreach (Room room in currentRoom.children)
        {
            path = room.searchRoom(room, targetRoom, path);

            if (path.Count > 0)
            {
                path.Push(this);
                return path;
            }
        }

        if (this == targetRoom)
        {
            path.Push(this);
            return path;
        }

        return path;
    }


    //Devuelve una habitación donde cavar de forma que quede repartido
    public Room getDiggingRoom()
    {
        Room winner = null;

        if(children.Count < maxChildren)
        {
            winner = this;
        }

        foreach (Room child in children)
        {
            Room candidate = child.getDiggingRoom();
            
            if (candidate != null)
            {
                if (winner == null)
                {
                    winner = candidate;
                }
                else if ((candidate.depth == winner.depth && candidate.children.Count < winner.children.Count) ||
                    candidate.depth < winner.depth)
                {
                    winner = candidate;
                }
            }
        }
        return winner;
    }
   

    //Devuelve el camino hasta la Room de ese tipo
    public Stack<Room> searchRoomType(RoomType type, Stack<Room> path, bool nonEmpty, bool nonFull)
    {
        foreach (Room room in children)
        {
            path = room.searchRoomType(type, path, nonEmpty, nonEmpty);

            if (path.Count > 0)
            {
                path.Push(this);
                return path;
            }
        }
        // El tipo coincide        comprobar nonEmpty si procede    comprobar nonFull si procede
        if (this.Type == type && !(Content < 2  && nonEmpty) && !(Content > maxCapacity && nonFull))
        {
            path.Push(this);
            return path;
        }

        return path;
    }


    //Devuelve directamente la Room que se ha encontrado sin el camino
    public Room getRoomType(RoomType type, bool nonEmpty, bool nonFull)
    {
        //foreach (Room room in children)
        //{
        //    Room candidate = room.getRoomType(type, nonEmpty, nonFull);

        //    if(candidate != null)
        //    {
        //        return candidate;
        //    }
        //}
        //// El tipo coincide        comprobar nonEmpty si procede    comprobar nonFull si procede
        //if (this.Type == type && !(Content < 2 && nonEmpty) && !(Content > maxCapacity && nonFull))
        //{
        //    return this;
        //}

        //return null;
        Room winner = null;

        if (this.Type == type && !(Content <= 0 && nonEmpty) && !(Content > maxCapacity && nonFull))
        {
            winner = this;
        }

        foreach (Room child in children)
        {
            Room candidate = child.getRoomType(type, nonEmpty, nonFull);

            if (candidate != null)
            {
                if (winner == null || 
                    (this.Type == type && !(nonEmpty && winner.Content > candidate.Content) && !(nonFull && winner.Content < candidate.Content)))
                {
                    winner = candidate;
                }
            }
        }
        return winner;
    }

    //Encuentra el camino de un nodo hasta la raíz
    public static Stack<Room> findWayOut(Room room, Stack<Room> path)
    {
        if (room.parent != null)
        {
            path = findWayOut(room.parent, path);
        }

        path.Push(room);

        return path;
    }

    public void setVisible(bool visible)
    {
        isVisible = visible;

        foreach(Room child in children)
        {
            child.setVisible(visible);
        }

        foreach(MeshRenderer rend in GetComponentsInChildren<MeshRenderer>(true))
        {
            rend.enabled = visible;
        }

        SpriteRenderer sr = GetComponentInChildren<SpriteRenderer>(true);
        if (sr != null)
            sr.enabled = visible;
        SpriteMask sm = GetComponentInChildren<SpriteMask>(true);
        if (sm != null)
            sm.enabled = visible;
    }
}
