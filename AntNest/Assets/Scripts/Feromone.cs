﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum FeromoneType
{
    FOOD,
    DANGER
}


public class Feromone : MonoBehaviour
{
    [SerializeField] double maxLifeTime = 10;
    
    public FeromoneType type;

    public int ant_id;

    public int index;

    public bool inside = false;

    public Vector3 direction;

    double lifeTime = 0;

    GameObject obj;

    Vector3 initialScale;

    // Start is called before the first frame update
    void Start()
    {
        initialScale = transform.localScale;
        obj = GetComponent<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        //if (obj.activeSelf){
            lifeTime += Time.deltaTime;

            float scale = (float)((maxLifeTime - lifeTime) / maxLifeTime);
            transform.localScale = new Vector3(scale, scale, scale);

            if (lifeTime >= maxLifeTime)
            {
                //TODO::Utilizar spawner
                //SetInactive();

                Destroy(this.gameObject);
            }
        //}
        
    }

    public void SetInactive()
    {
        GetComponent<Spawnable>().SetInactive();
        transform.localScale = initialScale;
        lifeTime = 0;
    }
}
