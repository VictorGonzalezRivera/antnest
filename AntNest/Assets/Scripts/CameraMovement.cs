﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    Vector3 surfacePos = new Vector3(4, 6, -6);
    Vector3 surfaceRot = new Vector3(45, 0, 0);

    Vector3 anthillPos = new Vector3(13, -2, 0);
    Vector3 anthillRot = new Vector3(0, -90, 0);

    float speed = 20;

    bool outside = true;

    Camera camera;

    // Start is called before the first frame update
    void Start()
    {
        camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        //Movimiento
        if (Input.GetMouseButton(1))
        {
            if (outside)
            {
                gameObject.transform.position -= new Vector3(Input.GetAxisRaw("Mouse X") * Time.deltaTime * speed,
                                           0.0f, Input.GetAxisRaw("Mouse Y") * Time.deltaTime * speed);
            }
            else
            {
                gameObject.transform.position -= new Vector3(0.0f, Input.GetAxisRaw("Mouse Y") * Time.deltaTime * speed,
                                          Input.GetAxisRaw("Mouse X") * Time.deltaTime * speed);
            }
           

        }

        // Zoom
        if (outside)
        {
            camera.fieldOfView -= Input.GetAxis("Mouse ScrollWheel") * speed;

            if (camera.fieldOfView < 20)
                camera.fieldOfView = 20;
            else if (camera.fieldOfView > 100)
                camera.fieldOfView = 100;
        }
        else
        {
            camera.orthographicSize -= Input.GetAxis("Mouse ScrollWheel") * speed/2;
            if (camera.orthographicSize < 2)
                camera.orthographicSize = 2;
            else if (camera.orthographicSize > 10)
                camera.orthographicSize = 10;
        }

    }

    public void goToSurface()
    {
        outside = true;
        camera.orthographic = false;
        transform.position = surfacePos;
        transform.eulerAngles = surfaceRot;
    }

    public void gotToAnthill(float Zpos)
    {
        outside = false;
        camera.orthographic = true;
        transform.position = new Vector3(anthillPos.x, anthillPos.y, Zpos);
        transform.eulerAngles = anthillRot;
    }
}
