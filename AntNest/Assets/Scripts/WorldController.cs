﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum AntKind
{
    QUEEN,
    DRONE,
    SOLDIER,
    COLLECTOR,
    NURSE
}

public class WorldController : MonoBehaviour
{
    private static WorldController instance_;
    public static WorldController Instance_
    {
        get
        {
            return instance_;
        }
        set
        {
            if(instance_ != null)
            {
                instance_ = value;
            }
        }
    }
    [HideInInspector]
    public bool pause = false;

    [Header("GameSettings")]
    public GameSettingsSO game_settings;
    //Editor variables
    public GameObject antmolde;
    public EnemyBehaviour enemyMolde;
    //Camera
    public CameraMovement cam;

    //Nest View UI
    public GameObject nestViewPanel;

    //Game state
    bool outside;
    //Game parameters
    float temperature;
    //Times an update is going to execute
    [Range(1, 10)]
    public int updateLoop;

    [Header("Hormigueros")]
    public List<Nest> nests;
    [HideInInspector]
    public List<Nest> activeNests = new List<Nest>();
    private int activeNestId = 0;
    private int selectedNestIndex = -1;

    public List<EnemyBehaviour> enemies = new List<EnemyBehaviour>();

    [HideInInspector]
    public List<AntBehaviour> noNestAnts = new List<AntBehaviour>();

    public List<SpawnerManager> spawnerManagers;

    private void Awake()
    {
        foreach(Nest n in nests)
        {
            n.gameObject.SetActive(false);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        updateLoop = game_settings.nUpdatesLoop;
        instance_ = this;
        outside = true;
        cam.goToSurface();

        InitFirstNest();
        initSpawnerManagers();

        nestViewPanel.SetActive(false);
    }

    void initSpawnerManagers()
    {
        for (int i = 0; i < spawnerManagers.Count; i++)
        {
            spawnerManagers[i].World = this;
        }
    }

    public void Pause()
    {
        pause = !pause;
    }

    AntBehaviour CreateNestlessAnt(AntKind ak)
    {
        AntBehaviour newAnt = Instantiate(game_settings.antPrefab).GetComponent<AntBehaviour>();
        newAnt.myNest = null;
        newAnt.id = -1;
        newAnt.gameObject.name = "ant_NULL-NEST_" + newAnt.id + "_" + ak.ToString();
        newAnt.kind = ak;
        newAnt.transform.position = Vector3.zero;

        newAnt.globalState = FSMGlobal.OUTSIDE;
        newAnt.outsideState = FSMOutside.WAITING;
        newAnt.insideState = FSMInside.WAITING;
        //Podria estar bien dar variabilidad a la vida, hambre, velocidad, etc al nacer
        return newAnt;
    }

    public Nest CreateNewNest(Vector3 spawnPos)
    {
        if (nests.Count > 0)
        {
            Nest nest = nests[0];
            nest.transform.position = spawnPos;
            nest.id = activeNestId++;
            if(activeNests.Count <= 0)
            {
                nest.transform.position = Vector3.zero;
            }
            else
            {
                //La ultima posicion? deberia ser otra, justamente
                //nest.transform.position = activeNests[activeNests.Count - 1].transform.position;
            }
           
            nests.RemoveAt(0);
            activeNests.Add(nest);
            nest.gameObject.SetActive(true);

            if (selectedNestIndex < 0)
            {
                selectedNestIndex = 0;
                setNestVisible(activeNests[0], true);
                updateUINestText();
            }

            return nest;
        }
        return null;
    }

    void InitFirstNest()
    {
        /*
        Nest nest = CreateNewNest();
      
        
        for(int i =0; i< 10; i++)
        {
            nest.GiveBirthTo(AntKind.COLLECTOR);
        }
        */
        noNestAnts.Add(CreateNestlessAnt(AntKind.QUEEN));

    }

    void Update()
    {
        HandleInput();

        for (int i = 0; i< updateLoop; i++)
        {
            UpdateBehaviour();
        }
    }

    void UpdateBehaviour()
    {

        if (!pause)
        {
            for (int i = activeNests.Count - 1; i >= 0; i--)
            {
                activeNests[i].SelfUpdate();
            }
            /*
            foreach (Nest n in activeNests)
            {
                n.SelfUpdate();
            }
            */
           
            for (int i = noNestAnts.Count - 1; i >= 0; i--)
            {
                noNestAnts[i].SelfUpdate();
            }
            /*
           foreach (AntBehaviour ant in noNestAnts)
           {
               ant.SelfUpdate();
           }
           */
            for (int i = enemies.Count-1; i >= 0; i--)
            {
                enemies[i].SelfUpdate();
            }
            foreach(SpawnerManager spawner in spawnerManagers)
            {
                spawner.SelfUpdate();
            }            
        }
    }

    void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            switchCameraPos();
        }
    }

    void switchCameraPos()
    {
        outside = !outside;
        if (outside)
        {
            cam.goToSurface();
            nestViewPanel.SetActive(false);

        }
        else
        {
            float zPos;
            if (selectedNestIndex < 0)
                zPos = 0;
            else
                zPos = activeNests[selectedNestIndex].transform.position.z;

            cam.gotToAnthill(zPos);

            nestViewPanel.SetActive(true);
        }
    }

    void setNestVisible(Nest nest, bool visible)
    {
        nest.isVisible = visible;

        nest.rootRoom.setVisible(visible);

        foreach(AntBehaviour ant in nest.activeAnts)
        {
            ant.visible = visible;
            if (!ant.isOutside())
                ant.setVisible(visible);
            else if (!visible)
                ant.setVisible(true);
        }
        nest.GetComponentInChildren<SpriteRenderer>().enabled = true;
    }

    void updateUINestText()
    {
        nestViewPanel.GetComponentInChildren<Text>(true).text = "Nest " + (selectedNestIndex + 1);
    }
  
    public void switchNestView( bool right)
    {
        if (selectedNestIndex >= 0 && activeNests.Count > 1) {

            setNestVisible(activeNests[selectedNestIndex], false);

            if (right)
            {
                if (selectedNestIndex >= activeNests.Count - 1)
                    selectedNestIndex = 0;
                else
                    selectedNestIndex++;
            }
            else
            {
                if (selectedNestIndex <= 0)
                    selectedNestIndex = activeNests.Count - 1;
                else
                    selectedNestIndex--;
            }

            cam.gotToAnthill(activeNests[selectedNestIndex].transform.position.z);

            setNestVisible(activeNests[selectedNestIndex], true);

            updateUINestText();
        }
    }
}
