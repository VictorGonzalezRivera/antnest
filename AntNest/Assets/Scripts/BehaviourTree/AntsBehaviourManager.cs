﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntsBehaviourManager :MonoBehaviour
{
    private static AntsBehaviourManager instance;

    public static AntsBehaviourManager Instance
    {
        get{return instance; }
        set {if (instance == null) instance = value;}
    }

    private void Awake()
    {
        if(instance == null)
            instance = this;
    }

    public BehaviourTree GetBehaviourTree(AntBehaviour ant)
    {
        switch(ant.kind)
        {
            case AntKind.COLLECTOR:
                return BehaviourTrees.collectorsTree;

            case AntKind.DRONE:
                if(BehaviourTrees.dronesTree != null) return BehaviourTrees.dronesTree;
                return BehaviourTrees.collectorsTree;

            case AntKind.NURSE:
                if (BehaviourTrees.nursesTree != null) return BehaviourTrees.nursesTree;
                return BehaviourTrees.collectorsTree;

            case AntKind.QUEEN:
                if (BehaviourTrees.queensTree != null) return BehaviourTrees.queensTree;
                return BehaviourTrees.collectorsTree;

            case AntKind.SOLDIER:
                if (BehaviourTrees.soldiersTree != null) return BehaviourTrees.soldiersTree;
                return BehaviourTrees.collectorsTree;
            default:
                return null;
        }
        
    }

}
