﻿
using System.Collections.Generic;
using UnityEngine;

public enum TreeType
{
    OR = 0,
    AND = 1,
    ALWAYS_TRUE = 2,
    ALWAYS_FALSE = 3
}

public delegate bool behaviourFunc(AntBehaviour ant);

public class BehaviourTree
{
    public TreeType treeType;
    bool negado;
    List<BehaviourTree> children = new List<BehaviourTree>();

    behaviourFunc behaviour;

    public BehaviourTree(TreeType type, behaviourFunc behaviour, List<BehaviourTree> children)
    {
        this.treeType = type;
        this.children = children;
        this.behaviour = behaviour;
    }

    public BehaviourTree(TreeType type, bool negado, behaviourFunc behaviour, List<BehaviourTree> children)
    {
        this.treeType = type;
        this.negado = negado;
        this.children = children;
        this.behaviour = behaviour;
    }

    public bool DoTree(AntBehaviour ant)
    {
        if(children != null && children.Count > 0)
        {
            foreach (BehaviourTree dt in children)
            {
                bool treeReturn = dt.DoTree(ant);
                if (treeType == TreeType.OR && treeReturn)
                {
                    if (negado) return false;
                    return true;
                }
                else if (treeType == TreeType.AND && !treeReturn)
                {
                    if (negado) return true;
                    return false;
                }
            }
            //Si ha recorrido todos los hijos siendo un arbol and, devuelve true, o false si es negado
            if (treeType == TreeType.AND)
            {
                if (negado) return false;
                return true;
            }
            //Si ha recorrido todos los hijos siendo OR devuelve false, o true si es negado
            else if(treeType == TreeType.OR)
            {
                if (negado) return true;
                return false;
            }
        }
        else
        {
            return behaviour.Invoke(ant);
        }
        
        if (treeType == TreeType.ALWAYS_FALSE) return false;
        else return true;   // if (treeType == TreeType.ALWAYS_TRUE)   
    }


    public void AddChild(BehaviourTree child)
    {
        children.Add(child);
    }


}
