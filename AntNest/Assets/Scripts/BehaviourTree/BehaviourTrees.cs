﻿using System.Collections.Generic;
using UnityEngine;

public static class BehaviourTrees
{
    #region Collectors
    public static BehaviourTree collectorsTree = new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
    {
        // -> (AND)
            new BehaviourTree(TreeType.AND, (AntBehaviour ant)=>{ return true; },
            new List<BehaviourTree>(){

                // Estoy fuera?
                new BehaviourTree(TreeType.ALWAYS_TRUE,
                    (AntBehaviour ant)=>{
                        return ant.isOutside();
                    },
                    null),

                // ? (OR)
                new BehaviourTree(TreeType.OR, (AntBehaviour ant)=>{ return true; },
                new List<BehaviourTree>(){

                        // -> (AND)
                        new BehaviourTree(TreeType.AND, (AntBehaviour ant)=>{ return true; },
                        new List<BehaviourTree>(){

                                // Llevo comida?
                                new BehaviourTree(TreeType.ALWAYS_TRUE,
                                    (AntBehaviour ant)=>{
                                        return ant.isCarryingFood();
                                    },
                                    null),

                                //Llevo comida al hormiguero
                                new BehaviourTree(TreeType.ALWAYS_TRUE,
                                (AntBehaviour ant)=>{
                                    ant.goToNest();
                                    return true;
                                },
                                null)
                        }),

                        // -> (AND)
                        new BehaviourTree(TreeType.AND, (AntBehaviour ant)=>{ return true; },
                            new List<BehaviourTree>(){

                                //Tengo hambre?
                                new BehaviourTree(TreeType.ALWAYS_TRUE,
                                    (AntBehaviour ant)=>{
                                        return ant.isHungry();
                                    },
                                    null),

                                //Ir al  hormiguero
                                new BehaviourTree(TreeType.ALWAYS_TRUE,
                                    (AntBehaviour ant)=>{
                                        ant.goToNest();
                                        return true;
                                    },
                                    null)
                            }),

                        // -> (AND)
                        new BehaviourTree(TreeType.AND,
                            (AntBehaviour ant)=>{ return true; },
                            new List<BehaviourTree>(){

                                //Hay comida cerca?
                                new BehaviourTree(TreeType.ALWAYS_TRUE,
                                    (AntBehaviour ant)=>{
                                        if (ant.isFoodNearby())
                                            return true;
                                        return false;
                                    },
                                    null),

                                //Ir a por la comida
                                new BehaviourTree(TreeType.ALWAYS_TRUE,
                                    (AntBehaviour ant)=>{
                                        ant.goToFood();
                                        return true;
                                    },
                                    null)

                            }
                        ),

                         // -> (AND)
                         new BehaviourTree(TreeType.AND,
                            (AntBehaviour ant)=>{ return true; },
                            new List<BehaviourTree>(){

                                //Hay feromonas de comida cerca?
                                new BehaviourTree(TreeType.ALWAYS_TRUE,
                                    (AntBehaviour ant)=>{
                                        if (ant.isFoodFeromoneNearby())
                                            return true;
                                        return false;
                                    },
                                    null),

                                //Ir a por la feromona
                                new BehaviourTree(TreeType.ALWAYS_TRUE,
                                    (AntBehaviour ant)=>{
                                        ant.goToFoodFeromone();
                                        return true;
                                    },
                                   null)

                            }
                        ),

                        //moverse aleatoriamente
                        new BehaviourTree(TreeType.ALWAYS_TRUE,
                            (AntBehaviour ant)=>{
                                ant.moveRandomly();
                                return true;
                            },
                            null)
                    }),
            }),

            // ? (OR)  Dentro del hormiguero
            new BehaviourTree(TreeType.OR,
            (AntBehaviour ant)=>{return true; }, 
            new List<BehaviourTree>()
            {

                // -> (AND)
                new BehaviourTree(TreeType.AND,
                (AntBehaviour ant)=>{ return true; },
                new List<BehaviourTree>(){

                     //Estoy en "modo cavar"?
                    new BehaviourTree(TreeType.ALWAYS_TRUE,
                    (AntBehaviour ant)=>{
                        return ant.isGoingToDig();
                    },
                    null),

                    //Pues a cavar
                    new BehaviourTree(TreeType.ALWAYS_TRUE,
                    (AntBehaviour ant)=>{
                        ant.startDiggingNewRoom();
                        return true;
                    },
                    null),
                }),

                // -> (AND)
                new BehaviourTree(TreeType.AND,
                (AntBehaviour ant)=>{ return true; },
                new List<BehaviourTree>(){

                    // Llevo comida?
                    new BehaviourTree(TreeType.ALWAYS_TRUE,
                    (AntBehaviour ant)=>{
                        return ant.isCarryingFood();
                    },
                    null),

                    // ? (OR)
                    new BehaviourTree(TreeType.OR,
                    (AntBehaviour ant)=>{return true; },
                    new List<BehaviourTree>()
                    {
                        // -> (AND)
                        new BehaviourTree(TreeType.AND,
                        (AntBehaviour ant)=>{ return true; },
                        new List<BehaviourTree>(){

                            // Estoy en la despensa?
                            new BehaviourTree(TreeType.ALWAYS_TRUE,
                            (AntBehaviour ant)=>{
                                return ant.isAtRoomType(RoomType.FOOD);
                            },
                            null),

                            // ? (OR)
                            new BehaviourTree(TreeType.OR,
                            (AntBehaviour ant)=>{return true; },
                            new List<BehaviourTree>()
                            {
                                // Dejar comida (Devuelve false si está llena)
                                new BehaviourTree(TreeType.ALWAYS_TRUE,
                                (AntBehaviour ant)=>{
                                    return ant.leaveFood();
                                },
                                null),

                                // Buscar la habitación con menos hijos
                                new BehaviourTree(TreeType.ALWAYS_TRUE,
                                (AntBehaviour ant)=>{
                                    ant.goToRoomToDig(RoomType.FOOD);         
                                    return true;
                                },
                                null),
                            }),
                        }),

                         // Ir a la despensa
                        new BehaviourTree(TreeType.ALWAYS_TRUE,
                        (AntBehaviour ant)=>{
                            ant.goToRoomType(RoomType.FOOD, false, true);
                            return true;
                        },
                        null)
                    }),

                   

                }),

                 // -> (AND)
                new BehaviourTree(TreeType.AND,
                (AntBehaviour ant)=>{ return true; },
                new List<BehaviourTree>(){

                    //Tengo hambre?
                    new BehaviourTree(TreeType.ALWAYS_TRUE,
                            (AntBehaviour ant)=>{
                                return ant.isHungry();
                            },
                            null),

                    // ? (OR)
                    new BehaviourTree(TreeType.OR,
                    (AntBehaviour ant)=>{return true; },
                    new List<BehaviourTree>()
                    {
                        // -> (AND)
                        new BehaviourTree(TreeType.AND,
                        (AntBehaviour ant)=>{ return true; },
                        new List<BehaviourTree>(){

                            // Estoy en la despensa?
                            new BehaviourTree(TreeType.ALWAYS_TRUE,
                                (AntBehaviour ant)=>{
                                    return ant.isAtRoomType(RoomType.FOOD);
                                },
                                null),

                            // Como (devuelve false si está vacía)
                            new BehaviourTree(TreeType.ALWAYS_TRUE,
                                (AntBehaviour ant)=>{
                                    return ant.takeFood();
                                },
                                null)
                        }),

                        // Busco una despensa que no esté vacía (devuelve false si no hay)
                        new BehaviourTree(TreeType.ALWAYS_TRUE,
                        (AntBehaviour ant)=>{
                            return ant.goToRoomType(RoomType.FOOD, true, false);
                        },
                        null),

                        // Me pongo en modo de "emergencia" a buscar comida
                        new BehaviourTree(TreeType.ALWAYS_TRUE,
                        (AntBehaviour ant)=>{
                            ant.setEmergencyMode();
                            return true;
                        },
                       null)

                    }),
                        
                }),

                // Salir del hormiguero
                new BehaviourTree(TreeType.ALWAYS_TRUE, 
                    (AntBehaviour ant) =>{
                        ant.goOutside();
                        return true;
                    },
                    null)
            })
        

        });

        #endregion
        #region Drones
        public static BehaviourTree dronesTree = null;
    #endregion
    #region Nurses
    public static BehaviourTree nursesTree = new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
    {
        //Nivel 1 --> estoy fuera?
         new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return ant.isOutside(); }, new List<BehaviourTree>(){}),

         //Nivel 1 --> Estoy dentro
         new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
         {
             new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
             {
                 new BehaviourTree(TreeType.AND, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                 {
                    new BehaviourTree(TreeType.ALWAYS_TRUE, (AntBehaviour ant) => {return ant.isHungry(); }, null), //Tengo hambre?
                    new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                    {
                        new BehaviourTree(TreeType.AND, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                        {
                            new BehaviourTree(TreeType.ALWAYS_TRUE, (AntBehaviour ant) => {return ant.isAtRoomType(RoomType.FOOD); }, null),
                            new BehaviourTree(TreeType.ALWAYS_TRUE, (AntBehaviour ant) => {return ant.takeFood(); }, null),
                        }),
                        new BehaviourTree(TreeType.ALWAYS_TRUE, (AntBehaviour ant) => {return ant.goToRoomType(RoomType.FOOD, true, false); }, null),
                    }),
                   
                 }),

                 new BehaviourTree(TreeType.AND, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                 {
                    new BehaviourTree(TreeType.OR, (AntBehaviour ant) => {
                        bool exist = ant.myNest.rootRoom.getRoomType(RoomType.EGGS,false,false) != null;
                        return exist;
                    }, null), //Existe eggs
                    new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                    {
                        new BehaviourTree(TreeType.AND, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                        {
                          new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                          {
                            new BehaviourTree(TreeType.AND, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                            {
                                new BehaviourTree(TreeType.OR, (AntBehaviour ant) => {
                                    return ant.isCarryingEgg();
                                }, null),//lleva huevo?
                                
                                //Comportamiento si lleva huevo --> Siempre da true (o deberia)
                                new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                                {
                                    new BehaviourTree(TreeType.AND, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                                    {
                                        new BehaviourTree(TreeType.OR, (AntBehaviour ant) => {
                                            return ant.isAtRoomType(RoomType.EGGS);
                                        }, null), //Estoy en eggs?
                                        new BehaviourTree(TreeType.OR, (AntBehaviour ant) => {
                                            Debug.Log("Dejar huevo"); ant.DeployEgg();
                                            return true ; }, null), //Dejar huevo

                                    }),
                                    new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { ant.goToRoomType(RoomType.EGGS,false,false); return true; }, null), //Ir a eggs
                                }),

                            }),

                            //Comportamiento si no lleva huevo --> da siempre true
                            new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                            {
                                //Si estoy en FOOD coger comida para reina
                                new BehaviourTree(TreeType.AND, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                                {
                                    new BehaviourTree(TreeType.ALWAYS_TRUE, (AntBehaviour ant) => {return ant.isAtRoomType(RoomType.FOOD); }, null), //Estoy en FOOD
                                    new BehaviourTree(TreeType.ALWAYS_TRUE, (AntBehaviour ant) => {return ant.pickUpFood(); }, null), //Pillo comida 
                                    new BehaviourTree(TreeType.ALWAYS_TRUE, (AntBehaviour ant) => {return ant.goToRoomType(RoomType.BIRTH, false, false); }, null), //Me la llevo a la reina
                                }),

                                new BehaviourTree(TreeType.OR, true, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                                {
                                    new BehaviourTree(TreeType.AND, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                                    {
                                        new BehaviourTree(TreeType.OR, (AntBehaviour ant) => {
                                            return ant.isAtRoomType(RoomType.BIRTH);
                                        }, null), //Estoy en birth?
                                        new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                                        {
                                            new BehaviourTree(TreeType.AND, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                                            {
                                                new BehaviourTree(TreeType.OR, (AntBehaviour ant) => {
                                                    return ant.isCarryingFood();
                                                }, null), //Llevo comida
                                                new BehaviourTree(TreeType.OR, (AntBehaviour ant) => {
                                                    ant.feedQueen(); return true;
                                                }, null), //Se la doy a la reina
                                            }),
                                            
                                            new BehaviourTree(TreeType.AND, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                                            {
                                                new BehaviourTree(TreeType.OR, (AntBehaviour ant) => {
                                                    return ant.myNest.getQueen().isHungry(); }, null), //La reina tiene hambre?

                                                new BehaviourTree(TreeType.OR, (AntBehaviour ant) => {
                                                    ant.goToRoomType(RoomType.FOOD, true, false);
                                                    return true; }, null), //Me voy a por comida para la reina
                                            }),

                                            new BehaviourTree(TreeType.OR, (AntBehaviour ant) => {
                                                Debug.Log("Recoger huevo");
                                                return ant.PickUpEgg();
                                            }, null), //Pillo huevo
                                        }),
                                    }),
                                    new BehaviourTree(TreeType.OR, (AntBehaviour ant) => {
                                        ant.goToRoomType(RoomType.BIRTH,false,false); return true;
                                    }, null),

                                }),
                            }),

                          }),
                        }),

                        new BehaviourTree(TreeType.OR, (AntBehaviour ant) => {
                            Debug.Log("Espero"); return true;
                        }, null),

                    }),


                 }),


                 new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                 {
                     new BehaviourTree(TreeType.AND, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                     {
                         new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return ant.isGoingToDig(); }, null),

                          new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { ant.startDiggingNewRoom(); return true; }, null),
                     }),

                     new BehaviourTree(TreeType.OR, (AntBehaviour ant) => {ant.goToRoomToDig(RoomType.EGGS); return true; }, null)
                 }),


             }),

             new BehaviourTree(TreeType.OR, (AntBehaviour ant) => {Debug.Log("Hormiga nurse con hambre"); return true; }, null)
         })

    });
    #endregion
    #region Queens
    public static BehaviourTree queensTree = new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return true; },
        //Nivel 0 -> OR
        new List<BehaviourTree>()
        {
                //Nivel 1
                //branch 0 (AND)
                new BehaviourTree(TreeType.AND, (AntBehaviour ant) => { return true; },

                    new List<BehaviourTree>()
                    {
                        //Nivel 2 estoy fuera?
                        new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return ant.isOutside(); }, new List<BehaviourTree>()),
                        
                        //Compruebo si me piro
                        new BehaviourTree(TreeType.AND,true, (AntBehaviour ant) => {return true; }, new List<BehaviourTree>()
                        {
                            new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return ant.myNest != null; }, new List<BehaviourTree>(){ }),

                            new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return !ant.isFirstQueen(); }, new List<BehaviourTree>(){ }),

                            new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { ant.LeaveNest(); return true; }, new List<BehaviourTree>(){ })
                        }),
                        

                        // Estoy fuera Nivel 2
                         new BehaviourTree(TreeType.AND, true, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                         {
                            //Nivel 3 -> Estoy en peligro?
                            new BehaviourTree(TreeType.OR,  (AntBehaviour ant) => { return ant.isEnemiesNearby(); }, new List<BehaviourTree>()),
                            //Corro del enemigo
                            new BehaviourTree(TreeType.ALWAYS_TRUE,  (AntBehaviour ant) => { ant.RunFromEnemy(); return true; }, new List<BehaviourTree>()),
                         }),

                         //Estoy fuera, no estoy en peligro (AND)
                         new BehaviourTree(TreeType.AND, true, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                         {
                             //Nivel 3 -> Tengo hormiguero?
                             new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return ant.DoIHaveNest(); }, new List<BehaviourTree>()),
                             //Me meto en el hormiguero
                             new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { ant.goToNest(); return true; }, new List<BehaviourTree>())
                         }),

                         //Estoy fuera, no estoy en peligro, no tengo nido (AND)
                         new BehaviourTree(TreeType.AND, true, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                         {
                             //Nivel 3
                             //Es buen sitio para crear hormiguero nuevo?
                             new BehaviourTree(TreeType.OR,  (AntBehaviour ant) => { return ant.IsGoodPlaceForNewNest(); }, new List<BehaviourTree>()),
                             new BehaviourTree(TreeType.ALWAYS_TRUE,  (AntBehaviour ant) => { ant.CreateNewNest(); return true; }, new List<BehaviourTree>()),
                         }),

                         //Estoy fuera, no estoy en peligro, no es buen lugar para crear hormiguero
                         new BehaviourTree(TreeType.ALWAYS_TRUE,  (AntBehaviour ant) => { ant.LookForNewNestPlace(); return true; }, new List<BehaviourTree>())
                    }),
                //branch 1 -> Esta dentro de hormiguero (AND)
                new BehaviourTree(TreeType.AND, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
            {
                //OR --> nivel 2
                 new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                 {
                    
                        //Compruebo si me piro
                     new BehaviourTree(TreeType.AND, (AntBehaviour ant) => {return true; }, new List<BehaviourTree>()
                     {
                         new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return !ant.isFirstQueen(); }, new List<BehaviourTree>(){ }),

                         new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { ant.LeaveNest(); return true; }, new List<BehaviourTree>(){ })
                     }),
                     

                        //Proceso de cavacion de la birth room --> Nivel 3
                     new BehaviourTree(TreeType.AND, (AntBehaviour ant) => {return true; }, new List<BehaviourTree>()
                     {
                         new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return ant.isGoingToDig(); }, new List<BehaviourTree>(){ }),

                         new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { ant.startDiggingNewRoom(); return true; }, new List<BehaviourTree>(){ })
                     }),


                     //AND --> nivel 3
                     new BehaviourTree(TreeType.AND, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                     {
                        //Tiene habitacion para dar a luz? --> Nivel 4
                        new BehaviourTree(TreeType.OR, (AntBehaviour ant) => {return !ant.BirthRoomExist(); }, new List<BehaviourTree>(){ }),

                         //No tengo habitacion, la creo --> Nivel 4
                        new BehaviourTree(TreeType.OR, (AntBehaviour ant) => {ant.goToRoomToDig(RoomType.BIRTH); return true; }, new List<BehaviourTree>(){ })

                     }),

                     //AND --> nivel 3
                     new BehaviourTree(TreeType.AND, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>()
                     {
                        //Estoy en la habitacion --> Nivel 4
                        new BehaviourTree(TreeType.OR, (AntBehaviour ant) => {return !(ant.isAtRoomType(RoomType.BIRTH)); }, new List<BehaviourTree>(){ }),

                        //Si no estoy voy --> Nivel 4
                        new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { ant.goToRoomType(RoomType.BIRTH, false, false); return true; }, new List<BehaviourTree>(){ })
                     }),

                    //Esta alli?
                    new BehaviourTree(TreeType.OR, (AntBehaviour ant) => {ant.GiveBirth(); return true; }, new List<BehaviourTree>(){ })

                 })
            })
    });

    #endregion Queens
    #region Soldiers
    public static BehaviourTree soldiersTree = new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>() {

        // -> (AND)
        new BehaviourTree(TreeType.AND, (AntBehaviour ant)=>{ return true; },
        new List<BehaviourTree>(){

                // Estoy fuera?
                new BehaviourTree(TreeType.ALWAYS_TRUE,
                (AntBehaviour ant)=>{
                    return ant.isOutside();
                },
                null),

                // ? (OR)
                new BehaviourTree(TreeType.OR, (AntBehaviour ant)=>{ return true; },
                new List<BehaviourTree>(){


                    new BehaviourTree(TreeType.AND, (AntBehaviour ant)=>{ return true; },
                    new List<BehaviourTree>(){

                        // Tengo hambre?
                        new BehaviourTree(TreeType.ALWAYS_TRUE,
                        (AntBehaviour ant)=>{
                            return ant.isHungry();
                        },
                        null),

                        // ir al hormiguero
                        new BehaviourTree(TreeType.ALWAYS_TRUE,
                        (AntBehaviour ant)=>{
                            ant.goToNest();
                            return true;
                        },
                        null)
                    }),

                    new BehaviourTree(TreeType.AND, (AntBehaviour ant)=>{ return true; },
                    new List<BehaviourTree>(){

                        // Hay feromonas de peligro?
                        new BehaviourTree(TreeType.ALWAYS_TRUE,
                        (AntBehaviour ant)=>{
                            return ant.isDangerFeromoneNearby();
                        },
                        null),

                        // ir a la feromona 
                        new BehaviourTree(TreeType.ALWAYS_TRUE,
                        (AntBehaviour ant)=>{
                            ant.goToDangerFeromone();
                            return true;
                        },
                        null)
                    }),

                    //moverse aleatoriamente
                    new BehaviourTree(TreeType.ALWAYS_TRUE,
                    (AntBehaviour ant)=>{
                        ant.moveRandomly();
                        return true;
                    },
                    null)
                }),

                
        }),
         // ? (OR)  Dentro del hormiguero
        new BehaviourTree(TreeType.OR,
        (AntBehaviour ant)=>{return true; }, 
        new List<BehaviourTree>()
        {
             // -> (AND)
                new BehaviourTree(TreeType.AND,
                (AntBehaviour ant)=>{ return true; },
                new List<BehaviourTree>(){

                    //Tengo hambre?
                    new BehaviourTree(TreeType.ALWAYS_TRUE,
                            (AntBehaviour ant)=>{
                                return ant.isHungry();
                            },
                            null),

                    // ? (OR)
                    new BehaviourTree(TreeType.OR,
                    (AntBehaviour ant)=>{return true; },
                    new List<BehaviourTree>()
                    {
                        // -> (AND)
                        new BehaviourTree(TreeType.AND,
                        (AntBehaviour ant)=>{ return true; },
                        new List<BehaviourTree>(){

                            // Estoy en la despensa?
                            new BehaviourTree(TreeType.ALWAYS_TRUE,
                                (AntBehaviour ant)=>{
                                    return ant.isAtRoomType(RoomType.FOOD);
                                },
                                null),

                            // Como (devuelve false si está vacía)
                            new BehaviourTree(TreeType.ALWAYS_TRUE,
                                (AntBehaviour ant)=>{
                                    return ant.takeFood();
                                },
                                null)
                        }),

                        // Busco una despensa que no esté vacía (devuelve false si no hay)
                        new BehaviourTree(TreeType.ALWAYS_TRUE,
                        (AntBehaviour ant)=>{
                            return ant.goToRoomType(RoomType.FOOD, true, false);
                        },
                        null),

                        // Me pongo en modo de "emergencia" a buscar comida
                        new BehaviourTree(TreeType.ALWAYS_TRUE,
                        (AntBehaviour ant)=>{
                            ant.setEmergencyMode();
                            return true;
                        },
                       null)

                    }),

                }),
                // Salir del hormiguero
                new BehaviourTree(TreeType.ALWAYS_TRUE,
                    (AntBehaviour ant) =>{
                        ant.goOutside();
                        return true;
                    },
                    null)  
        })
    });
    #endregion

    public static BehaviourTree CreateCollectorsTree()
    {
        BehaviourTree root = new BehaviourTree(TreeType.ALWAYS_TRUE, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>());
        BehaviourTree llevoComida = new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return ant.isCarryingFood(); }, new List<BehaviourTree>());
        BehaviourTree estoyFuera = new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return ant.isOutside(); }, new List<BehaviourTree>());
        BehaviourTree estoyEnPeligro = new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return ant.isEnemiesNearby(); }, new List<BehaviourTree>());
        BehaviourTree feromonoPeligro = new BehaviourTree(TreeType.ALWAYS_TRUE, (AntBehaviour ant) => { ant.leaveFeromone(FeromoneType.DANGER); return true; }, new List<BehaviourTree>());
        BehaviourTree irAlHormiguero = new BehaviourTree(TreeType.ALWAYS_TRUE, (AntBehaviour ant) => { ant.goToNest(); return true; }, new List<BehaviourTree>());
        BehaviourTree moverseAleatoriamente = new BehaviourTree(TreeType.ALWAYS_TRUE, (AntBehaviour ant) =>{ ant.moveRandomly(); return true; }, new List<BehaviourTree>());

        //Crear ANDs y ORs vacios para incluirlos al arbol
        BehaviourTree root_AND0 = CreateEmptyAND();
            BehaviourTree root_AND_OR = CreateEmptyOR();
                BehaviourTree root_AND_OR_AND0 = CreateEmptyAND();
                    BehaviourTree root_AND_OR_AND0_OR = CreateEmptyOR();
                        BehaviourTree root_AND_OR_AND0_OR_AND = CreateEmptyAND();
                BehaviourTree root_AND_OR_AND1 = CreateEmptyAND();
        BehaviourTree root_AND1 = CreateEmptyAND();

        //Creamos el arbol a partir de todos los behaviourtrees
        //Nivel 0
            //Rama izquierda
            root.AddChild(root_AND0);
            //Rama derecha
            root.AddChild(root_AND1);
        //Nivel 1
        root_AND0.AddChild(llevoComida);
        root.AddChild(root_AND_OR);

        root_AND1.AddChild(moverseAleatoriamente);
        //Nivel 2
        root_AND_OR.AddChild(root_AND_OR_AND0);
        root_AND_OR.AddChild(root_AND_OR_AND1);
        //Nivel 3
            //Rama 0
            root_AND_OR_AND0.AddChild(estoyFuera);
            root_AND_OR_AND0.AddChild(root_AND_OR_AND0_OR);
            //Rama 1 -->> FALTA POR IMPLEMENTAR
            //root_AND_OR_AND1.AddChild();
        //Nivel 4
        root_AND_OR_AND0_OR.AddChild(root_AND_OR_AND0_OR_AND);
        root_AND_OR_AND0_OR.AddChild(irAlHormiguero); //Esto es llevar comida al hormiguero
        //Nivel 5
        root_AND_OR_AND0_OR_AND.AddChild(estoyEnPeligro);
        root_AND_OR_AND0_OR_AND.AddChild(feromonoPeligro);
        return root;
    }
    static BehaviourTree CreateEmptyAND()
    {
        return new BehaviourTree(TreeType.AND, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>());
    }

    static BehaviourTree CreateEmptyOR()
    {
        return new BehaviourTree(TreeType.OR, (AntBehaviour ant) => { return true; }, new List<BehaviourTree>());
    }
}
